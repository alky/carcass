<?php
// HTTP
 $host = $_SERVER['HTTP_HOST'];

define('HTTP_SERVER', 'http://'.$host.'/private/new/');

// HTTPS
define('HTTPS_SERVER', 'http://'.$host.'/private/new/');

// DIR
$dir = dirname(__FILE__);
define('DIR_APPLICATION', 	$dir . '/catalog/');
define('DIR_SYSTEM', 		$dir . '/system/');
define('DIR_TEMPLATE', 		$dir . '/catalog/view/theme/');
define('DIR_CONFIG', 		$dir . '/system/config/');
define('DIR_IMAGE', 		$dir . '/image/');
define('DIR_CACHE', 		$dir . '/system/storage/cache/');
define('DIR_LOGS', 		$dir . '/system/storage/logs/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', '');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'test');
define('DB_PORT', '3306');
define('DB_PREFIX', 'do_');

