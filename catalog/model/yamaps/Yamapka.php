<?php

namespace Model\yamaps;

class Yamapka extends \System\Model {
        
    function getAllAddBlue($data) {
        $sql = 'SELECT DISTINCT fs.ID, fs.NAME, fs.GPSLAT, fs.GPSLNG, fs.WEBCOMMENT';
        $sql .= ' FROM ZDB.LIFUELSTATIONSETPRODUCT#V fp';
        $sql .= ' LEFT JOIN ZDB.LIBPRODUCT#V gp ON gp.ID = fp.PRODUCTID';
        $sql .= ' LEFT JOIN ZDB.LIFUELSTATION#V fs ON fs.ID = fp.FUELSTATIONID';
        $sql .= " WHERE gp.PARENTID = '1475338'";
        $sql .= ' AND (select count (1) from ZDB.WEBDISABLEDFS#V r where r.fuelstationid = fp.id) = 0';
        $sql .= " AND fp.ACTIVE = 'Y'";
        $sql .= " AND fs.gpslat between ${data['gpslat-f']} AND ${data['gpslat-t']}";
        $sql .= " AND fs.gpslng between ${data['gpslng-f']} AND ${data['gpslng-t']}";
        $sql .= " AND fs.statusid = '233024'";
        
        return $this->oracle->query($sql); 
    }
    
    function getAllStations($data) {
        
        $sql = 'SELECT min(x.id) id, x.name, x.gpslat, x.gpslng, x.webcomment';
        $sql .= ' FROM (SELECT'; 
        $sql .= ' b.id, b.name, b.gpslat, b.gpslng, b.webcomment';
        $sql .= ' FROM ZDB.LIFUELSTATION#V b';
        $sql .= ' WHERE b.sclassid = 1840 AND b.statusid = 233024'; 
        $sql .= ' AND (select count (1) from ZDB.WEBDISABLEDFS#V r where r.sclassid = 5693 and r.fuelstationid = b.id) = 0';
        $sql .= " AND b.gpslat between ${data['gpslat-f']} AND ${data['gpslat-t']}";
        $sql .= " AND b.gpslng between ${data['gpslng-f']} AND ${data['gpslng-t']}";
        $sql .= ' ) x group by x.name, x.gpslat, x.gpslng, x.webcomment';



        return $this->oracle->query($sql);        
    }

    function getFilteredStations($data) {
        
        $exceptions = $this->getExceptions($data);
        
        $sql = "SELECT min(x.id) AS id, x.name, x.pricenal, x.${data['tariff_name']}, x.gpslat, x.gpslng, x.webcomment";
        $sql .= ' FROM (SELECT';
        $sql .= " b.id, p.pricenal, p.${data['tariff_name']}, b.name, b.gpslat, b.gpslng, b.webcomment";
        $sql .= ' FROM ZDB.LIFUELSTATION#V b';
        $sql .= ' LEFT JOIN ZDB.LIPRICELIST#V p ON b.id = p.fuelstationid';
        $sql .= ' LEFT JOIN ZDB.LIBPRODUCT#V pr ON  p.PRODUCTID = pr.ID';
        $sql .= ' LEFT JOIN ZDB.LIBPRODUCTGROUP#V g ON pr.PARENTID = g.ID'; 
        $sql .= ' WHERE b.sclassid = 1840 AND b.statusid = 233024';
        
        $sql .= $exceptions;
        
        
        $data['maxprice'] ? $sql .= " AND p.${data['tariff_name']} <= ${data['maxprice']}" : '';
        $sql .= ' AND (select count (1) from ZDB.WEBDISABLEDFS#V r where r.sclassid = 5693 and r.fuelstationid = b.id) = 0';
        $sql .= " AND b.gpslat between ${data['gpslat-f']} AND ${data['gpslat-t']}";
        $sql .= " AND b.gpslng between ${data['gpslng-f']} AND ${data['gpslng-t']}";
        $sql .= " ) x group by x.name, x.pricenal, x.${data['tariff_name']}, x.gpslat, x.gpslng, x.webcomment";        
        
        return $this->oracle->query($sql);
        
    }
    
    
    private function getExceptions($data) 
    {
        $sql_str = '';
        
        //Для фильтров берем только Россию
        $sql_str .= ' AND p.COUNTRYID = 212178';
        
        //Тариф Эксклюзивная (PRICEEXCL) цена действует только на диз.топливо (124320)
        
        if ($data['tariff_name'] == 'PRICEEXCL') {
            $sql_str .= ' AND g.PARENTID = 124320';
        } else {
            $data['ftype'] ? $sql_str .= " AND g.PARENTID = ${data['ftype']}" : '';
        }
        
        return $sql_str;
    }

    function getStationData($data) {
        
         if (isset($data['tarif_name'])) {

             $columns = "p.FUELSTATIONID_FULLNAME as name, p.REGIONID_FULLNAME as region, p.PRODUCTID_FULLNAME as product, g.PARENTID_FULLNAME as maincat, g.NAME as catname";
             
                 if ($data['tarif_percent']) {

                     $discount = 'p.DISC_PRICEEXCL as discount';

                 } else {

                     $discount = "(p.PRICENAL - p.${data['tarif_name']}) as discount";
                 }
             
             $columns .= ", p.${data['tarif_name']} as price, $discount";
             
             $sql = "SELECT $columns FROM ZDB.LIPRICELIST#V p"
                . " LEFT JOIN ZDB.LIBPRODUCT#V pr ON  p.PRODUCTID = pr.ID"
                . " LEFT JOIN ZDB.LIBPRODUCTGROUP#V g ON pr.PARENTID = g.ID "
                . " WHERE FUELSTATIONID = ${data['id']}";
                
            $result = $this->oracle->query($sql);

         } else {
             
             $sql1 = "SELECT p.FUELSTATIONID_FULLNAME as name, p.REGIONID_FULLNAME as region,"
                     . " p.PRODUCTID_FULLNAME as product, g.PARENTID_FULLNAME as maincat,"
                     . " g.NAME as catname, p.PRICENAL as price FROM ZDB.LIPRICELIST#V p"
                        . " LEFT JOIN ZDB.LIBPRODUCT#V pr ON  p.PRODUCTID = pr.ID"
                        . " LEFT JOIN ZDB.LIBPRODUCTGROUP#V g ON pr.PARENTID = g.ID "
                        . " WHERE FUELSTATIONID = ${data['id']}";             
             
             $result = $this->oracle->query($sql1);
             
             //Если станции нет в прайсе выводим только название/адрес
             if (!$result->row) {
                $sql = "SELECT (NAME || ' ' || ADDRESS) AS NAME, REGIONID_FULLNAME AS REGION FROM ZDB.LIFUELSTATION#V WHERE ID = ${data['id']}";
                $result = $this->oracle->query($sql);
             }
         }        
        
         return $result;
        
    }
    
}
