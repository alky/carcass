var map,
        objectManager,
        mapType = 'yandex#map',
//        controls;
        lastroute = null,
        lastpoint = null;




function init_map() {
    
    var msk = [55.75, 37.62];
    
    //шаблон (hint)

    var MyHintContentLayout = ymaps.templateLayoutFactory.createClass(
      "<b>{{ properties.hintContent.i_name }}</b><br>" +
      "[if properties.hintContent.i_discount]<span style='color:red'>Скидка: {{ properties.hintContent.i_discount }} {{ properties.hintContent.i_type }}</span>[endif]"
    );
    ymaps.layout.storage.add('fshint', MyHintContentLayout);
    


            objectManager = new ymaps.LoadingObjectManager('index.php?route=common/home/lom&t=p&bbox=%b', {
                clusterize: true,
                clusterHasBalloon: true,
                openHintOnHover: true,
                openBalloonOnClick: false,
                splitRequests: false
            });

            function addIc(obj, icon, title) {
                obj.append($('<div>').attr('title', title).addClass('lk-infra bg-deeporange').append($('<div>').append($('<i>').addClass('lk-icon fg-white icon-' + icon))));
            }

            function addIcb(obj, icon, badge, title) {
                obj.append($('<div>').attr('title', title).addClass('lk-infra bg-deeporange').append($('<div>')
                        .append($('<i>').addClass('lk-icon fg-white icon-' + icon))
                        .append($('<i>').addClass('lk-icon fg-white lk-icon-badge icon-' + badge))
                        ));
            }

            objectManager.objects.events.add('click', function (e) {
                
                handleBaloonClick(e);
                
            });    

            map = new ymaps.Map('map', {
                center: msk,
                zoom: 7,
                type: 'yandex#map',
                controls: [] // "default", "routeEditor"
            }, {
                minZoom: 4,
                yandexMapDisablePoiInteractivity: true
            });


            map.events.add('contextmenu', function (e) {
                if (!map.balloon.isOpen()) {
                     var coords = e.get('coords'),
                        crds = [coords[0].toPrecision(6),coords[1].toPrecision(6)],
                        crdspano = [coords[0].toPrecision(8),coords[1].toPrecision(8)],
                        svurl = 'http://maps.google.com/maps?q=&layer=c&cbll=' + parseFloat(crdspano[0])  + ','+ parseFloat(crdspano[1]),
                        button_from = '<button class="baloobut" style="float:left" onclick="routeFrom(\''+ crds +'\');map.balloon.close()">Отсюда</button>',
                        button_pano = '<button class="baloobut" style="float:left;margin-left:3px;margin-right:3px;width:70px" onclick="window.open(\''+ svurl +'\', \'_blank\')">Панорама</button>',
                        button_to = '<button class="baloobut" style="float:right" onclick="routeTo(\''+ crds +'\');map.balloon.close()">Сюда</button>';
                    map.balloon.open(coords, {
                       contentBody:'<p>Можно проложить маршрут:</p>',
                       contentFooter: button_from + button_pano + button_to
                    });
                    

                    
                }
                else {
                    map.balloon.close();
                }
            });
            

    map.geoObjects.add(objectManager);
  
}

function onReloadObjectManager(callback) {
      var ev_num = 0;  
      var cur_state = 0;
      objectManager.objects.overlays.events.add('add', function (event) {
            ev_num++;
            if (ev_num > 1 && cur_state != 1) {
                cur_state = 1;
                setTimeout(callback,500);
            }
      });
}

function doFilter(controls){
 
    objectManager.setUrlTemplate('index.php?route=common/home/lom&t=p&' + controls + '&bbox=%b');
        if (lastroute) objectManager.setFilter('');
        
    objectManager.reloadData();
    ymaps.modules.require(['PieChartClustererLayout'], function (PieChartClustererLayout) {

        onReloadObjectManager(function(){
            objectManager.options.set({clusterIconLayout: PieChartClustererLayout}); 
            if (lastroute) _doRoute();
        });

    });
}            

function routeFrom(crds) {
    let coords = crds.split(',');
    geoRoute(true);
    $('#map-route-from input').val(coords[0] + ',' + coords[1]).focus();
    
}

function routeTo(crds) {
    let coords = crds.split(',');
    geoRoute(true);
    $('#map-route-to input').val(coords[0] + ',' + coords[1]).focus();
}

function getStationsAlongRoute (route) {

    var area = $('#map-select-distance').attr('data-distance') || 500;
    
//var t0 = performance.now();
    
    if (!route.isArray) {
        var segments = [];
        route.getPaths().toArray().forEach(path => {
            path.getSegments().forEach(segment => {
                segments.push(...segment.getCoordinates());
            });
        });
    } else {
        var segments = route;
    }
    
        polyline = new ymaps.Polyline(segments);
        polyline.options.setParent(map.options);
        polyline.geometry.setMap(map);

        objectManager.objects.each(function(obj){
            var objCoords = obj.geometry.coordinates;
            var closest = polyline.geometry.getClosest(objCoords);
           
            obj.properties.route = '';
            
            if (closest.distance < area) {
                obj.properties.route = 'alongRoute';
            }
        });
    
    if (typeof latestFilter != 'undefined') {
        objectManager.setFilter(latestFilter + ' && properties.route == "alongRoute"');
    } else {
        objectManager.setFilter('properties.route == "alongRoute"');
    }        

        
//var t1 = performance.now();
//console.log('Segments', segments.length, '/ Perfomance now = ', t1-t0);

}

function mtCl(ar) {
    mapType = ar.mid;
    $('#mapTypeBtn').find('i').attr('class', 'lk-icon').addClass(ar.icon);
    map.setType(mapType);
}

function geoClick() {
    var btn = $(event.target).closest('button');
    if ($('#mapTypeBtn-popup').length !== 0) {
        $('#mapTypeBtn-popup').lkButtonSet('close');
    } else {
        $('<div>').attr('id', 'mapTypeBtn-popup').appendTo($('body')).lkButtonSet({
            buttons: [
                {
                    icon: 'icon-map-map',
                    x: btn.offset().left + (btn.width() - 40) / 2,
                    y: btn.offset().top + btn.height() + 8,
                    backgroundColor: '#FF5722',
                    rippleColor: '#FFAB91',
                    hint: 'Карта - схема',
                    onClick: function (event) {
                        mtCl({mid: "yandex#map", icon: "icon-map-map"});
                    }
                },
                {
                    icon: 'icon-map-satellite',
                    x: btn.offset().left + (btn.width() - 40) / 2,
                    y: btn.offset().top + btn.height() + 48 + 8,
                    backgroundColor: '#FF5722',
                    rippleColor: '#FFAB91',
                    hint: 'Снимки со спутника',
                    onClick: function (event) {
                        mtCl({mid: "yandex#satellite", icon: "icon-map-satellite"});
                    }
                },
                {
                    icon: 'icon-map-hybrid',
                    x: btn.offset().left + (btn.width() - 40) / 2,
                    y: btn.offset().top + btn.height() + 48 * 2 + 8,
                    backgroundColor: '#FF5722',
                    rippleColor: '#FFAB91',
                    hint: 'Гибридная карта',
                    onClick: function (event) {
                        mtCl({mid: "yandex#hybrid", icon: "icon-map-hybrid"});
                    }
                }
            ]
        });
    }
}

function _doRoute() {
    map.geoObjects.remove(lastpoint);
    map.geoObjects.remove(lastroute);
    lastroute = null,
            lastpoint = null;
    var $this1 = $('#map-route-from'), _options1 = $this1.data('lkEditor'),
            $this2 = $('#map-route-to'), _options2 = $this2.data('lkEditor'), _from, _to;
    _from = _options1.geoPoint;
    if (_from.length === 0) {
        _from = $this1.lkEditor('dbValue');
    }
    _to = _options2.geoPoint;
    if (_to.length === 0) {
        _to = $this2.lkEditor('dbValue');
    }
    if ((_from.length === 0) || (_to.length === 0)) {
        $('#map-route-form .lk-map-route-distance').html('Необходимо указать начало и конец маршрута..');
        return false;
    }
    ymaps.route([_from, _to], {
        mapStateAutoApply: true
    }).then(function (route) {
        var routeLength = route.getHumanLength();
        
        $('#map-route-form .lk-map-route-distance').html('');
        $('#map-route-form .lk-map-route-distance').html('расстояние: ' + routeLength);
        lastroute = route;
        if (route.getPaths().length === 0) {
            $('#map-route-form .lk-map-route-distance').html('нет дороги...');
        }
//  console.log('Маршурут готов: ', route);
        map.geoObjects.add(route);
        var s = route.getPaths().get(0).getSegments();
        var pp = [];
        var sc = 0;
        
        $(s).each(function () {
            
            var se = this.getCoordinates();
            $(se).each(function () {
                if (sc % 10 === 0) {
                    pp.push([Math.round(this[0] * 1000000) / 1000000, Math.round(this[1] * 1000000) / 1000000]);
                }
                sc++;
            });
        });
//        if (route.getLength() > 3000000) {
//            getStationsAlongRoute(pp);
//        } else {
//            getStationsAlongRoute(route);
//        }
//        var area = $('#distance-selector option:selected').attr()
        getStationsAlongRoute(route);
        
        return route;
        
    }, function (error) {
        var errs = {500: 'Не удалось построить маршрут.'};
        $('#map-route-form .lk-map-route-distance').html('Ошибка: ' + errs[error.code]);
    });
    
}

function _doJump() {
    map.geoObjects.remove(lastpoint);
    map.geoObjects.remove(lastroute);
    lastroute = null,
            lastpoint = null;

    var $this1 = $('#map-route-from'), _options1 = $this1.data('lkEditor'), pos,
            $this2 = $('#map-route-to'), _options2 = $this2.data('lkEditor');
    pos = _options1.geoPoint;
    if (pos.length === 0) {
        pos = $this1.lkEditor('dbValue');
    }
    if (pos.length === 0) {
        pos = _options2.geoPoint;
    }
    if (pos.length === 0) {
        pos = $this2.lkEditor('dbValue');
    }
    if (pos.length === 0) {
        $('#map-route-form .lk-map-route-distance').html('Необходимо указать хотя-бы одну из точек..');
        return false;
    }

    ymaps.geocode(pos).then(function (geores) {
        if (geores.geoObjects.getLength() !== 0) {
            var addr = geores.geoObjects.get(0).properties.get('metaDataProperty'),
                    geom = geores.geoObjects.get(0).geometry.getCoordinates();
            $('#map-route-form .lk-map-route-distance').html(addr.GeocoderMetaData.text);
            lastpoint = new ymaps.Placemark(geom, {hintContent: addr.GeocoderMetaData.text}, {preset: 'islands#darkOrangeGovernmentIcon'});
            map.geoObjects.add(lastpoint);
            map.panTo(geom);
        } else {
            $('#map-route-form .lk-map-route-distance').html('Не удалось ничего найти...');
        }
    });
}

function geoRoute(s) {
    if ($('#map-route-form').length !== 0) {
        if ($('#map-route-form').css('display') === 'none') {
            $('#map-route-form').fadeIn(400 * GLOBAL_ANIMATE);
        } else {
            if (s !== true) {
                map.geoObjects.remove(lastroute);
                $('#map-route-form').fadeOut(400 * GLOBAL_ANIMATE);
                lastroute = null;
                if (typeof latestFilter != 'undefined') {
                    objectManager.setFilter(latestFilter);
                } else {
                    objectManager.setFilter('');
                }
            }
        }
    } else {
        var distance_options = '<option data-distance="500" name="500">500 м</option>'
                                + '<option data-distance="1" name="1">1 км</option>'
                                + '<option data-distance="5" name="5">5 км</option>';
        var $div = $('<div>').attr('id', 'map-route-form');
        $div.append($('<div>').addClass('map-route-tail'));
        $div.append($('<div>').addClass('map-route-header').html('Построение маршрута'));
        $div.append($('<div>').addClass('map-route-close-btn').append($('<button>').attr('title', 'Закрыть').attr('id', 'map-route-closebtn')));
        $div.append($('<div>').addClass('lk-over-grid')
                .append($('<div>').addClass('lk-grid')
                        .append($('<div>').addClass('lk-grid--row')
                                .append($('<div>').addClass('lk-grid--col').addClass('w2').attr('id', 'map-route-from'))
                                .append($('<div>').addClass('lk-grid--col').addClass('w2').attr('id', 'map-route-to'))
                                )
                        )
                )
                .append($('<div>').addClass('map-route-overfooter')
                        .append($('<table>').addClass('map-route-footer')
                                .append($('<tr>')
                                        .append($('<td>')
                                                .append($('<div>').addClass('lk-map-route-distance')))
                                        .append($('<td>')
                                                .append($('<button>').attr('id', 'map-route-jump').attr('title', 'Перейти к указанной точке').html('ПЕРЕЙТИ'))
                                                .append($('<button>').attr('id', 'map-route-search').attr('title', 'Построить маршрут между точками').html('ПОСТРОИТЬ'))
                                                .append($('<button>').attr('id', 'map-select-distance').attr('title', 'Удаленность от маршрута')
                                                    .addClass("lk-button-icon-right").html('Удаленность 500 м<i class="lk-icon icon-arrow-down"></i>'))
                                                )
                                        )
                                 
                                )
                        );
                
        $div.appendTo($('body')).fadeIn(400 * GLOBAL_ANIMATE);
        $('#map-route-closebtn').lkButton({type: 'icon', ripple: false, icon: 'icon-close-big', onClick: function () {
                geoRoute(false);
            }});
        $('#map-route-from').lkEditor({caption: 'Точка отправления', suggest: true, onSuggest: _doSugg, onSuggested: _doSuggd});
        $('#map-route-from input').on('keypress', function (event) {
            if (event.which === 13) {
                _doJump();
            }
        });
        $('#map-route-to').lkEditor({caption: 'Точка назначения', suggest: true, onSuggest: _doSugg, onSuggested: _doSuggd});
        $('#map-route-to input').on('keypress', function (event) {
            if (event.which === 13) {
                _doRoute();
            }
        });
        $('#map-route-jump').lkButton({type: 'flat', onClick: _doJump});
        $('#map-route-search').lkButton({type: 'flat', onClick: _doRoute});
        $('#map-select-distance').lkButton({type: 'flat', onClick: selectDistance});
    }
}

function selectDistance() {
    var popupY = $('#map-select-distance').offset().top;
    var popupX = $('#map-select-distance').offset().left;    
    var itms = [
        {caption:"Удаленность 500 м",mid:"500"},
        {caption:"Удаленность 1000 м",mid:"1000"},
        {caption:"Удаленность 5000 м",mid:"5000"}
    ];    
    $('<div id="ds-pop">').appendTo('body');
    
    $('#ds-pop').lkPopup({
        items: itms,
        x: popupX ,
        y: popupY + 38,
        onClick: function(i) {
            $('#map-select-distance').attr({
                                'data-distance' : i.mid,
                                'name': i.name
                                })
                                  .html(i.caption + '<i class="lk-icon icon-arrow-down"></i>');
            
        }
    });
}

function _doSugg() {
    var $this = $(this), _options = $this.data('lkEditor'),
            $sugg = $this.find('.lk-editor--suggest'),
            v = $this.lkEditor('dbValue').trim();
    if (v.length === 0) {
        $sugg.find('*').off().remove();
        $sugg.css({display: 'none'});
        _options.priorRequest = v;
        _options.geoPoint = '';
        $this.find('.lk-editor--info').html('');
        return;
    }
    if (_options.priorRequest !== v) {
        ymaps.geocode(v, {results: 5}).then(function (geores) {
            var sr = ['hydro'];
            if (geores.geoObjects.getLength() !== 0) {
                $sugg.find('*').off().remove();
                _options.geoPoint = '';
                $this.find('.lk-editor--info').html('');
                for (var i = 0; i < geores.geoObjects.getLength(); i++) {
                    var geom = geores.geoObjects.get(i).geometry.getCoordinates(),
                            addr = geores.geoObjects.get(i).properties.get('metaDataProperty').GeocoderMetaData,
                            ac = addr.Address.Components,
                            kind = addr.kind,
                            fn = addr.text,
                            ln = ac[ac.length - 1].name;
                    if (fn.substr(fn.length - ln.length, ln.length) === ln) {
                        fn = fn.substr(0, fn.length - ln.length - 2);
                    }
                    if (sr.indexOf(kind) === -1) {
                        $sugg.append($('<div>').attr('title', addr.text).addClass('lk-suggest--item').attr('geo-point', geom[0] + ',' + geom[1])
                                .append($('<div>').html(ac[ac.length - 1].name))
                                .append($('<div>').html(fn))
                                );
                    } else {
//                        console.log(kind, addr.text);
                    }
                }
                $sugg.css({display: 'block'});
            } else {
                _options.geoPoint = '';
                $this.find('.lk-editor--info').html('');
                $sugg.find('*').off().remove();
                $sugg.css({display: 'none'});
                return;
            }
        });
        _options.priorRequest = v;
    }
}

function _doSuggd() {
    var $this = $(this), _options = $this.data('lkEditor'),
            $e = $this.find('.lk-suggest--item.active');
    if ($e.length === 0) {
        return;
    }
    _options.priorRequest = $e.find('div:first-child').html();
    $this.lkEditor('dbValue', $e.find('div:first-child').html());
    _options.geoPoint = $e.attr('geo-point');
    $this.find('.lk-editor--info').html($e.find('div:last-child').html());
//    console.log($e.find('div:first-child').html(), $e.find('div:last-child').html(), $e.attr('geo-point'));
}

function _trySave() {
    var res = '', vars = '';
    res += 'var mapCenter=[' + map.getCenter() + '];var mapZoom=' + map.getZoom() + '; function init_map(){';
    res += 'map=new ymaps.Map(\'map\',{center:mapCenter,zoom:mapZoom,type:\'yandex#map\',controls:[]},{minZoom:4,yandexMapDisablePoiInteractivity: true});';
    if ($('#map-route-form').length !== 0) {
        vars += 'var lastSearch=' + JSON.stringify({_from: $('#map-route-from').lkEditor('dbValue'), _to: $('#map-route-to').lkEditor('dbValue')}) + ';';
    }
    if (lastroute !== null) {
        vars += 'var lastRoute=' + JSON.stringify(lastroute.requestPoints) + ';';
        res += 'ymaps.route(lastRoute,{mapStateAutoApply:true}).then(function(route){map.geoObjects.add(route);});';
    }
    if (lastpoint !== null) {
        vars += 'lastPoint=' + JSON.stringify({point: lastpoint.geometry.getCoordinates(), hintContent: lastpoint.properties.get('hintContent'), preset: lastpoint.options.get('preset')}) + ';';
        res += 'map.geoObjects.add(new ymaps.Placemark(lastPoint.point, {hintContent: lastPoint.hintContent}, {preset: lastPoint.preset}));';
    }
    vars += 'var objectManager;var map;';
    res += 'objectManager=new ymaps.ObjectManager({clusterize:true,clusterHasBalloon:false,openHintOnHover:true,openBalloonOnClick:false,preset:\'islands#darkOrangeClusterIcons\'});';
    res += 'objectManager.add(stations);'
    vars += 'var stations=' + JSON.stringify(objectManager.objects.getAll()) + ';';
    res += 'map.geoObjects.add(objectManager);'
    res += '};';
    res += '$(function(){$(\'#map\').height($(window).height());$(window).resize(function(){$(\'#map\').height($(window).height());});ymaps.ready(init_map);';
    res += '$(\'.lk-editor--input\').attr(\'disabled\', \'disabled\');'; // '$(\'.lk-editor\').addClass(\'disabled\');';
    res += '$(\'.lk-button--icon\').attr(\'disabled\', \'disabled\');';
    res += '$(\'.lk-button--flat\').attr(\'disabled\', \'disabled\');';
    if ($('#map-route-form').length !== 0) {
        res += '$(\'#map-route-form input\').val(lastSearch._from);';
        res += '$(\'#map-route-to input\').val(lastSearch._to);';
    }
    res += '});';
//    res += '</script>';
    var xxx = $('body').html().replace('"map"', '"map-2"');
    var $xxx = $('<div>').html(xxx);
    $xxx.find('#map-2').html('');
//    res += $xxx.html().replace('"map-2"', '"map"').trim() + '</body>';
//    vars = '<html><head><title>Panda - сеть АЗС</title><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><script type="text/javascript" src="/js/jquery-3.2.1.min.js"></script><script src="https://api-maps.yandex.ru/2.1.56/?lang=ru_RU" type="text/javascript"></script><link href="/css/application-normal.min.css" rel="stylesheet" type="text/css"/></head>' + vars;
    $.ajax({
        type: 'post',
        url: 'index.php?route=common/home/guestmap&guestmap_id=0',
        data: {
//            html: vars + res + '</html>'
                f: {vars: vars,
                    res: res
                }
        },
        dataType: 'json',
        success: function (data) {
            $('<div>').addClass('lk-taskbar').appendTo($('body'));
            $('<div>').addClass('lk-dialog').attr('id', 'dialog').appendTo($('body'));

            $('#dialog').append($('<div>').addClass('lk-grid')
                    .append($('<div>').addClass('lk-grid--row')
                            .append($('<div>').attr('id', 'text').appendTo('#dialog').addClass('lk-grid--col w1'))
                            )
                    );
            $('#text').lkEditor({type: 'strlong', caption: 'Текст ссылки для отправки контрагенту', fixheight: 96});
            $('#text').lkEditor('dbValue', location + 'index.php?route=common/home/guestmap&' + data);
            $('#dialog').lkWindow({
                title: 'Информация',
                modal: true,
                move: true,
                resize: true,
                sysbuttons: {
                    minimize: {},
                    maximize: {},
                    close: {
                        onClick: function () {
                            $('.animate').removeClass('animate');
                            $('#dialog').lkWindow('destroy');
                        }
                    }
                },
                onResize: function () {
                    var $page = $('#dialog');
                    var $memo = $page.find('#text');
                    var nh = $page.innerHeight() - ($memo.find('textarea').parent().outerHeight() - $memo.find('textarea').outerHeight()); // $page.innerHeight(); // /* - $memo.parent().position().top - ($memo.parent().outerHeight() */ - $memo.find('textarea').outerHeight();
//                    console.log(nh);
                    $memo.find('textarea').css({
                        'min-height': nh,
                        'max-height': nh,
                        height: nh
                    });
                },
                buttons: [
                    {caption: 'Закрыть', onClick: function () {
                            $('.animate').removeClass('animate');
                            $('#dialog').lkWindow('destroy');
                        }
                    }
                ]
            }).lkWindow('center').lkWindow('show');
            $('#dialog').find('textarea').focus();
        }
    });
    
}

function lomReady(lom) {
    
    if(lom) { 
        objectManager.objects.add(lom);
    }         
    
}  

function showFilters() {
        
    if ($('#map-filter').parent().hasClass('lk-window')) {
        $('#map-filter').lkWindow('destroy');
        return false;
    }        

    var ewidth = 600;
    var eleft = $(window).width() - ewidth - 10;
    $('#map-filter').lkWindow({
        title: 'Фильтры',
        modal: false,
        move: false,
        resize: true,
        height: 700,
        top: 90,
        width: ewidth,
        left: eleft,

        sysbuttons: {
            close: {
                onClick: function () {
                    $('.animate').removeClass('animate');
                    $('#map-filter').lkWindow('destroy');
                    $('.sppb-btn').remove();
                }
            }
        },
        buttons: [
            {caption: 'Закрыть', onClick: function () {
                    $('.animate').removeClass('animate');
                    $('#map-filter').lkWindow('destroy');
                    $('.sppb-btn').remove();
                }
            }
        ]
    }).lkWindow('show');
    
    var exel_button = '<a class="sppb-btn sppb-btn-sm sppb-btn-success" download="fuelstations-discount.xls" href="#" onclick="toExel(this)">Скачать в Excel</a>'
    
    $(exel_button).appendTo($('.lk-window--footer'));
    $('#map-filter').toggle();
    getControlElements();
}
function toExel(el) {
    if ($('#fsgt td').length == 0 ){
        alert('Не выбраны заправки!');
        return false;
    }
    return ExcellentExport.excel(el, 'fsgt', 'Список АЗС');
}

function modifyTableHeader(type) {
    let thead = '<thead>'
           +'<tr><th width="60%">AЗС</th>'
           +'     <th width="1px">Регион</th>';
    
    switch(type) {
        //ДТ
        case '124320':
            thead += '<th data-sort="float" class="sorting-asc">ДТ ТУ</th>'
                    +'<th data-sort="float" class="">ДТ ЗИМА</th>'
                    +'<th data-sort="float" class="">ДТ ЛЕТО</th>'
                    +'<th data-sort="float" class="">ДТ ЕВРО</th>'
                    +'<th data-sort="float" class="">Скидка*</th>';
        break;
        //Бензин
        case '124322':
            thead += '<th data-sort="float" class="">АИ-92</th>'
                    +'<th data-sort="float" class="">АИ-95</th>'
                    +'<th data-sort="float" class="">АИ-98</th>'
                    +'<th data-sort="float" class="">Скидка*</th>';
        break;   
        //Газ
        case '259093':
            thead += '<th data-sort="float" class="sorting-asc">СПГ (метан)</th>'
                    +'<th data-sort="float" class="">Газ (пропан)</th>'
                    +'<th data-sort="float" class="">КПГ</th>'
                    +'<th data-sort="float" class="">Скидка*</th>';
        break;
        
        default:
        break;
        }
        
        thead += '</tr></thead>';
        $('#fsgt thead').remove();
        $('#fsgt tbody').html('');
        $(thead).prependTo($('#fsgt'));
        
//   return html;
}

function handleBaloonClick(elem) {

    var objectId = elem.get('objectId'),

        t_id = $("#tariff-select").attr("data-tarifid") || '',
        t_txt = $("#tariff-select").text(),
        ftype = $("#ftype-select").attr("data-ftype") || '';    
        
        if (t_txt == 'Тариф' || !t_txt) t_txt = 'Цена на стелле';
        
        $.get('index.php?route=common/home/lom&t=i&id=' + objectId + '&tariff=' + t_id + '&ftype=' + ftype).done(function (data) {
        var e = '<div>'+ data.row.NAME +'</div>'
                + '<div>'+ data.row.REGION +'</div>';
            
            var addToTable = false;
            if (t_id != '' && ftype != '' && t_id != 'all') addToTable = true;

            var saved = document.querySelector('#fsgt tbody');
            var saved_row = '<tr id="'+objectId+'" onclick="rmTr(this)"><td>' + data.row.NAME + '</td><td>' + data.row.REGION + '</td>';
            var thead = '<table class="table striped hovered" style="width:100%"><hr><thead><tr><td>Топливо</td><td>Цена</td></tr></thead>';
            var tboby = '';
            var discount = [];
            var emptytd = '<td>-</td>';
            
            var td1,td2,td3,td4,td5,td6,td7,tddiscount,postfix;
            
            data.rows.forEach(function(el){
                if (el.PRICE) tboby += '<tr><td>'+el.PRODUCT+'</td><td>' + el.PRICE + '</td></tr>';
            
                if (addToTable) {
                
                    var catname = el.CATNAME.replace(/^\s+|\s+$/g, '');
                    var maincat = el.MAINCAT.replace(/^\s+|\s+$/g, '');

                    if (maincat == 'дизель' && ftype == 124320) {

                    switch(catname) {
                        case 'ДТ ТУ':
                            td1 = '<td>' + el.PRICE + '</td>';
                            break;
                        case 'ДТ зима':
                            td2 = '<td>' + el.PRICE + '</td>';
                            break;
                        case 'ДТ лето':
                            td3 = '<td>' + el.PRICE + '</td>';
                            break;
                        case 'ДТ евро':
                            td4 = '<td>' + el.PRICE + '</td>';
                            break;
                        default:
                            break;
                    }

                        if (el.DISCOUNT) discount.push(parseFloat(el.DISCOUNT.replace(/[-%\s]/g, '').replace(',', '.')));   
                    }

                    if (maincat == 'бензин' && ftype == 124322) {
                    switch(catname) {
                        case 'АИ-92':
                            td1 = '<td>' + el.PRICE + '</td>';
                            break;
                        case 'АИ-95':
                            td2 = '<td>' + el.PRICE + '</td>';
                            break;
                        case 'АИ-98':
                            td3 = '<td>' + el.PRICE + '</td>';
                            break;
                        default:
                            break;
                    }
                        if (el.DISCOUNT) discount.push(parseFloat(el.DISCOUNT.replace(/[-%\s]/g, '').replace(',', '.')));   
                    }

                    if (maincat == 'газ' && ftype == 259093) {
                    switch(catname) {
                        case 'СПГ (метан)':
                            td1 = '<td>' + el.PRICE + '</td>';
                            break;
                        case 'Газ (пропан)':
                            td2 = '<td>' + el.PRICE + '</td>';
                            break;
                        case 'КПГ':
                            td3 = '<td>' + el.PRICE + '</td>';
                            break;
                        default:
                            break;
                    }
                    if (el.DISCOUNT) discount.push(parseFloat(el.DISCOUNT.replace(/[-%\s]/g, '').replace(',', '.')));   
                    }        
                }
             });
             if (addToTable) {
                t_id === '78983640' ? postfix = ' %' : postfix = ' руб';

                if (discount) tddiscount = '<td>' + Math.max.apply(null, discount).toFixed(2) + postfix + '</td>';  

                if (ftype == 124320) saved_row += (td1 || emptytd) + (td2 || emptytd) + (td3 || emptytd) + (td4 || emptytd) + (tddiscount || emptytd) + '</tr>';

                if (ftype == 124322) saved_row += (td1 || emptytd) + (td2 || emptytd) + (td3 || emptytd) + (tddiscount || emptytd) +'</tr>';

                if (ftype == 259093) saved_row += (td1 || emptytd) + (td2 || emptytd) + (td3 || emptytd) + (tddiscount || emptytd) +'</tr>';
            
            
                
                var preset = objectManager.objects.getById(objectId).options.preset,
                    oldpreset = objectManager.objects.getById(objectId).options.oldpreset,
                    saved_html = saved.innerHTML;

                if (preset != 'islands#darkOrangeCircleDotIcon') {
                    objectManager.objects.setObjectOptions(objectId, {preset: 'islands#darkOrangeCircleDotIcon', oldpreset: preset, hideIconOnBalloonOpen: false});
                    saved.innerHTML = saved_html + saved_row;
                } else {
                    let tr = document.getElementById(objectId);
                    if (tr) tr.remove();
                    objectManager.objects.setObjectOptions(objectId, {preset: oldpreset});
                }
            }
            e += thead + '<tbody>' + tboby + '</tbody><tfoot><tr><td colspan="2">Тариф: '+ t_txt +'</td></tr></tfoot></table>';
            objectManager.objects.getById(objectId).properties.balloonContent = e;
            objectManager.objects.balloon.open(objectId);          

    });
}

function getControlElements() {
    
    if ($('#tariff-select').length  == 0) { 
    
        var request = $.ajax({
            url: 'index.php?route=common/home/getTariff',
            contentType: "application/json"
        });

        request.done(function(response){
            var resp = response;
            var popupY = $('#control-form').offset().top;
            var popupX = $('#control-form').offset().left;
//пока adblue добавляю так
            resp.ftypes.items.push({caption:"AddBlue",mid:"addblue"});
console.log(response);            
            $('<button id="ftype-select" class="lk-button-icon-right">Тип топлива<i class="lk-icon icon-arrow-down"></i></button>').lkButton({
                backgroundColor: '#fff',
                captionColor: '#808080',
                onClick:function(){
                    $('<div id="fs-pop">').appendTo('body');
                    $('#fs-pop').lkPopup({
                        items: resp.ftypes.items,
//    items: [{caption:'fuel',items:['1', '2', '3']},'other'],
                        x: popupX + 370,
                        y: popupY + 38,
                        onClick: function(i) {
                            $('#ftype-select').attr('data-ftype', i.mid).html(i.caption + '<i class="lk-icon icon-arrow-down"></i>');
                            applyFilter();
                        }
                    });
                }
            }).prependTo('#control-form');             
    
    resp.tariffs.items.push({caption:"Цена на стелле",mid:"all"});
            
            $('<button id="tariff-select" class="lk-button-icon-right">Тариф<i class="lk-icon icon-arrow-down"></i></button>').lkButton({
                backgroundColor: '#fff',
                captionColor: '#808080',
                onClick:function(){
                    $('<div id="ts-pop">').appendTo('body');
                    $('#ts-pop').lkPopup({
                        items: resp.tariffs.items,
                        x: popupX ,
                        y: popupY + 38,
                        onClick: function(i) {
                            $('#tariff-select').attr({
                                                'data-tarifid' : i.mid,
                                                'name': i.name
                                                })
                                                  .html(i.caption + '<i class="lk-icon icon-arrow-down"></i>');
                            applyFilter();              
                        }
                    });
                }
            }).prependTo('#control-form');

        });
    
    }
}

function applyFilter() {
        var tarif_id = $('#tariff-select').attr('data-tarifid');
        var ftype = $('#ftype-select').attr('data-ftype');
        var maxprice = $('#price-filter input').val();
        controls = 'tariff=' + tarif_id + '&ftype=' + ftype + '&maxprice=' + maxprice;
        if (tarif_id == 78983640) {
            $('#min_d_val').parent().show();
            $('#ftype-select').prop('disabled', true).attr('data-ftype', '124320').html('Дизель<i class="lk-icon icon-arrow-down"></i>');
            modifyTableHeader('124320');
        } else {
            $('#min_d_val').parent().hide();
            $('#min_d_val input, #max_d_val input').val('');
            $('#ftype-select').prop('disabled', false);
        }
        
        
        if (ftype) {
            modifyTableHeader(ftype);
            if (ftype == 'addblue') {
                $('#tariff-select').prop('disabled', true).attr('data-tarifid', '').html('Тариф<i class="lk-icon icon-arrow-down"></i>');;
                controls = 'ftype=' + ftype;
            } else {
                $('#tariff-select').prop('disabled', false);
            }
        } 
    
        if (ftype && tarif_id) {
            $('#price-filter').parent().show();
        } else {
            $('#price-filter').parent().hide();
        }
        
        doFilter(controls);
        

}

function rmTr(el) {
    $(el).remove();
}


$(function () {
    $('body').append($('<div>').attr('id', 'map').css({width: '100%', height: $(window).height()}));
    $(window).resize(function () {
        $('#map').height($(window).height());
    });
    ymaps.ready(init_map);
    $('#loading').remove();
    
    $('body').append($('<div>').addClass('lk-map--ctrl-right-top').append($('<button>').addClass('lk-map--iface-btn').attr('id', 'mapTypeBtn').attr('title', 'Выбор вида карты').lkButton({
        type: 'fab',
        icon: 'icon-map-map',
        backgroundColor: '#FF5722',
        rippleColor: '#FFAB91',
        onClick: geoClick
    })));
    $('body').append($('<div>').addClass('lk-map--ctrl-left-top').append($('<button>').addClass('lk-map--iface-btn').attr('id', 'mapRouteBtn').attr('title', 'Построение маршрута').lkButton({
        type: 'fab',
        icon: 'icon-map-route',
        backgroundColor: '#FF5722',
        rippleColor: '#FFAB91',
        onClick: geoRoute
    })));
    $('body').append($('<div>').addClass('lk-map--ctrl-left-bottom').append($('<button>').addClass('lk-map--iface-btn').attr('id', 'mapFilters').attr('title', 'Фильтры').lkButton({
        type: 'fab',
        icon: 'icon-search',
        backgroundColor: '#FF5722',
        rippleColor: '#FFAB91',
        onClick: showFilters
    })));
    
    
    $('<div style="position:absolute;bottom:0px;right:0px;"><button id="save"></button></div>').appendTo($('body'));
    $('#save').lkButton({type: 'icon', icon: 'icon-lock', onClick: _trySave});
    

   $('#control-form').append($('<div>').addClass('lk-over-grid')
        .append($('<div>').addClass('lk-grid')
                .append($('<div style="display:none">').addClass('lk-grid--row')
                        .append($('<div>').addClass('lk-grid--col').addClass('w2').attr('id', 'min_d_val'))
                        .append($('<div>').addClass('lk-grid--col').addClass('w2').attr('id', 'max_d_val'))
                        )
                .append($('<div style="display:none">').addClass('lk-grid--row')
                        .append($('<div>').addClass('lk-grid--col').addClass('w1').attr('id', 'price-filter'))
                        )                        
                )
        );
    $('#min_d_val').lkEditor({caption: 'Скидка от ....'});
    $('#max_d_val').lkEditor({caption: 'Скидка до ....'});
    $('#price-filter').lkEditor({caption: 'Найти АЗС с ценой ниже чем ...'});
    
    $('#price-filter input').on('change', function(){
        applyFilter();
    });
    
    $('#min_d_val input, #max_d_val input').on('change', function() {
        
        var d_min = $('#min_d_val input').val();
        var d_max = $('#max_d_val input').val();
    
        if (d_min != '' && d_max != '') {
        
        var filterStr = 'properties.discount > ' + d_min + ' && properties.discount < ' + d_max;
        latestFilter = filterStr;
        
            if ( lastroute != null ) {
                getStationsAlongRoute(lastroute);
            } else {
                objectManager.setFilter(filterStr);
            }        
        }
    });    
    
    
});


//function panoramaInit(crds) {
//    if (crds) {
//    var panoholder = document.createElement("div");
//        panoholder.id = "pano";
//        panoholder.style = {
//            width: "100%",
//            height: "100%",
//            float:  "left", 
//            "z-index" : 1
//        };
//    document.body.appendChild(panoholder);
//
//        $('#pano').lkWindow({
//            title: 'Google street view presents',
//            modal: false,
//            move: false,
//            resize: false,
//            height: 700,
//            top: 90,
//            width: 700,
//            left: 150,
//
//            sysbuttons: {
//                minimize: {},
//                maximize: {},
//                close: {
//                    onClick: function () {
//                        $('.animate').removeClass('animate');
//                        $('#pano').lkWindow('destroy');
//                    }
//                }
//            },
//            buttons: [
//                {caption: 'Закрыть', onClick: function () {
//                        $('.animate').removeClass('animate');
//                        $('#pano').lkWindow('destroy');
//                        killpano();
//                        return false;
//                        
//                    }
//                }
//            ]
//        }).lkWindow('show');    
//    var coords = crds.split(',');
////console.log('coords in panorama ', coords, ' parse float coords ', parseFloat(coords[1]).toPrecision(9));    
//    var panorama;
//    var clickpoint = {lat: parseFloat(coords[0]), lng: parseFloat(coords[1])};
//console.log(clickpoint);    
//    var sv = new google.maps.StreetViewService();
//    panorama = new google.maps.StreetViewPanorama(document.getElementById('pano'));
//    sv.getPanorama({location: clickpoint, radius: 50}, processSVData);
//    
//      function processSVData(data, status) {
//console.log(data);          
//        if (status === 'OK') {
//
//          panorama.setPano(data.location.pano);
//          panorama.setPov({
//            heading: 270,
//            pitch: 0
//          });
//          panorama.setVisible(true);
//console.log(panorama);
//        } else {
//          console.error('Street View data not found for this location.');
//        }
//      }    
//    
//    }
//}
//
//function killpano() {
//    setTimeout(function(){
//     $('#pano').remove();
//    }, 3000);
//}