;
(function ($) {
    /*
     * @onClick                     function(this, item)
     * 
     */
    var options = 'lkPopup';
    var POPUP_INTERVAL = 400;
    var POPUP_ANIMATE = 400;
    var methods = {
        create: function (options_set) {
            return this.each(function () {
                var $this = $(this), _options, $menu;
                if (!($this.data(options))) {
                    $this.data(options, $.extend({}, {x: $(window).width() / 2, y: $(window).height() / 2, items: []}, options_set));
                    _options = $this.data(options);
                    _options.pm = {dx: 1, dy: 1};
                    _options.mi_pl = lk.getCssProp('.lk-menu--outer .lk-menu--inner .lk-menu--item .icon-pre', 'width');
                    _options.mi_pr = lk.getCssProp('.lk-menu--outer .lk-menu--inner .lk-menu--item .icon-post', 'width');
                    $menu = $(_get_menu($this, _options.items, 1));
                    $this.addClass('lk-popupmenu').append($menu);

                    $this.find('.icon-post').closest('.lk-menu--item').off('click');
                    $menu.css({
                        left: _options.x,
                        top: _options.y
                    });
                    $('<div>').addClass('modal-front lk-popup--back').insertBefore($this).lkResizable({
                        onResize: function () {
                            $('.lk-popup--back').css({
                                height: $('body').prop('scrollHeight') + 'px'
                            });
                        }
                    });
                    $menu.fadeIn(POPUP_ANIMATE * GLOBAL_ANIMATE, function () {
                        $menu.addClass('open');
                    })
                    if ($menu.offset().top + $menu.outerHeight() > $(window).height() - 2) {
                        $menu.css({
                            top: $(window).height() - 2 - $menu.outerHeight()
                        });
                        _options.pm.dy = -1;
                    }
                    if ($menu.offset().left + $menu.outerWidth() > $(window).width() - 2) {
                        $menu.css({
                            left: $(window).width() - 2 - $menu.outerWidth()
                        });
                        _options.pm.dx = -1;
                    }
                    $this.find('.lk-menu--item:not(.disabled):not(.separator)').on({
                        mouseenter: __item__mouseenter,
                        click: __item__click
                    });
                    $this.find('.icon-post>.icon-menu-open').closest('.lk-menu--item').off('click');
                    $this.on({
                        contextmenu: false
                    });
                    $('.lk-popup--back').on({
                        mousedown: __out__menu
                    });
                    $(window).resize();
                }
            });
        },

        close: function () {
            return this.each(function () {
                var $this = $(this), _options = $this.data(options);
                if (_options) {
                    clearTimeout(_options.hovertimer);
                }
                $($this.find('*')).off('*').removeData().remove();
                $this.off('*').removeData().remove();
                $('.lk-popup--back').removeData().remove();
            });
        }
    };

    $.fn.lkPopup = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else {
            if (typeof method === 'object' || !method) {
                return methods.create.apply(this, arguments);
            } else {
                $.error('lkPopup: method ' + method + ' not found');
            }
        }
    };

    function _get_menu(_this, _items, _level) {
        var $this = $(_this), _options = $this.data(options), res = '<div class="lk-menu--outer" style="z-index:' + _level + ';"><div class="lk-menu--inner">', ic_left = false, ic_right = false, _style = '';
        for (var i = 0; i < _items.length; i++) {
            if ((_items[i].icon) && (_items[i].icon.length !== 0)) {
                ic_left = true;
            }
            if ((_items[i].items) && (_items[i].items.length !== 0)) {
                ic_right = true;
            }
        }
        _style += ic_left ? ('padding-left:' + _options.mi_pl + ';') : '';
        _style += ic_right ? ('padding-right:' + _options.mi_pr + ';') : '';
        for (var i = 0; i < _items.length; i++) {
            if ($.type(_items[i]) === 'string') {
                _items[i] = {caption: _items[i]};
            }
            if (_items[i].caption === '-') {
                res += '<div class="lk-menu--item separator">';
            } else {
                _items[i].guid = lk.getGUID();
                res += '<div guid="' + _items[i].guid + '" class="lk-menu--item unselectable' + (((_items[i].disabled === true) || (_items[i].enabled === false)) ? ' disabled' : '') + '"' + ((_style === '') ? '' : (' style="' + _style + '"')) + '>';
                if ($.type(_items[i]) === "string") {
                    res += '<span class="caption">' + _items[i] + '</span>';
                } else {
                    if ((_items[i].caption) && (_items[i].caption.length !== 0)) {
                        res += '<span class="caption">' + _items[i].caption + '</span>';
                    }
                    if ((_items[i].icon) && (_items[i].icon.length !== 0)) {
                        res += '<span class="icon-pre"><i class="lk-menu--item-icon ' + _items[i].icon + '"></i></span>';
                    }
                    if ((_items[i].items) && (_items[i].items.length !== 0)) {
                        res += '<span class="icon-post"><i class="icon-menu-open"></i></span>';
                        res += _get_menu($this, _items[i].items, _level + 1);
                    }
                }
            }
            res += '</div>';
        }
        res += '</div></div>';
        return res;
    }

    function __item__hover(_this) {
        var $this = $(_this), _options = $this.data(options), $item, $itemmenu, $menu;
        if (!_options) {
            return;
        }
        $item = $this.find('[guid=' + _options.currItem + ']');
        if (!$item) {
            return;
        }
        if ($item.length === 0) {
            return;
        }

        $itemmenu = $item.closest('.lk-menu--outer');
        $menu = $item.children('.lk-menu--outer');
        $itemmenu.find('.lk-menu--item').find('.lk-menu--outer').removeClass('open');
        if ($menu.length === 0) {
            return;
        }
        $menu.addClass('open');

        if (_options.pm.dx === 1) {
            $menu.css({
                left: $item.offset().left + $item.outerWidth()
            });
            if ($menu.offset().left + $menu.outerWidth() > $(window).width() - 2) {
                $menu.css({
                    left: $item.offset().left - $menu.outerWidth()
                });
                _options.pm.dx = -1;
            }
        } else {
            $menu.css({
                left: $item.offset().left - $menu.outerWidth()
            });
            if ($menu.offset().left < 2) {
                $menu.css({
                    left: 2
                });
                _options.pm.dx = 1;
            }
        }

        if (_options.pm.dy === 1) {
            $menu.css({
                top: $item.offset().top - parseInt($menu.find('.lk-menu--inner').css('padding-top'))
            });
            if ($menu.offset().top + $menu.outerHeight() > $(window).height() - 2) {
                $menu.css({
                    top: $(window).height() - 2 - $menu.outerHeight()
                });
                _options.pm.dy = -1;
            }
        } else {
            $menu.css({
                top: $item.offset().top - $menu.outerHeight() + $item.outerHeight() + parseInt($menu.find('.lk-menu--inner').css('padding-top'))
            });
            if ($menu.offset().top < 2) {
                $menu.css({
                    top: 2
                });
                _options.pm.dy = 1;
            }
        }
    }

    function __item__mouseenter(event) {
        var $this = $(event.target).closest('.lk-popupmenu'), _options = $this.data(options), $item = $(event.target).closest('.lk-menu--item:not(.disabled):not(.separator)');
        if ($item.length === 0) {
            return;
        }
        $item.closest('.lk-menu--outer').find('.hover').removeClass('hover');
        $item.addClass('hover');
        clearTimeout(_options.hovertimer);
        _options.currItem = $item.attr('guid');
        _options.hovertimer = setTimeout(function () {
            __item__hover($this);
        }, POPUP_INTERVAL);
    }

    function _get_item(items, guid) {
        var res = null;
        for (var i = 0; i < items.length; i++) {
            if (items[i].guid === guid) {
                return items[i];
            }
            if ((items[i].items) && (items[i].items.length !== 0)) {
                res = _get_item(items[i].items, guid);
                if (res !== null) {
                    return res;
                }
            }
        }
        return res;
    }

    function __item__click(event) {
        var $this = $(event.target).closest('.lk-popupmenu'), _options = $this.data(options), $item = $(event.target).closest('.lk-menu--item:not(.disabled):not(.separator)'), guid = $item.attr('guid'), _method = _options.onClick, _res;
        event.stopPropagation();
        if (_method) {
            _res = _get_item(_options.items, guid);
            delete _res.guid;
            _method.call($this, _res);
        }
        methods['close'].apply($('.lk-popupmenu'));
    }

    function __out__menu(event) {
        event.stopPropagation();
        methods['close'].apply($('.lk-popupmenu'));
    }

})(jQuery);