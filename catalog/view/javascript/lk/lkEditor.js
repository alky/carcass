;
(function ($) {
    /*
     * @type            data type ( bool string integer float date datetime strlong text )
     * @icon            editor icon ( icon class )
     * @dbfield         dbfield name 
     * @caption         caption of editor
     * @maxlength       maximum length for text fields ( string strlong )
     * @nullable        check box can be null value ( true/false )
     * @notnull         editor must have value
     * @ripple          use ripple effect for checkbox
     * @changetime      timeout before trigger change event
     * 
     * @onChange        trigger when value changed with @changetime timeout
     //     * @onEnter         
     //     * @onExit          
     * @onSuggest       value change => new suggest list
     * @onSuggested     selected suggested value
     */

    var _BLUR_TIMEOUT = 150;

    var options = 'lkEditor';

    var methods = {

        create: function (options_set) {
            var default_edit = {
                type: 'string',
                icon: '',
                dbfield: '',
                caption: '',
                changetime: 0,
                ripple: true,
                nullable: false,
                notnull: false,
                digits: 2,
                suggest: false
            };
            return this.each(function () {
                var $this = $(this), _options = $this.data(options);
                if (!($this.data(options))) {
                    if (!$this.is('div'))
                        $.error('lkEditor: create - element is not div');
                    if (['string', 'integer', 'float', 'date', 'datetime', 'strlong', 'text', 's-dict'].indexOf($.extend({}, default_edit, options_set).type) === -1)
                        $.error('lkEditor: unknown element type: ' + options_set.type);
                    if (!_options)
                        _options = {};
                    if ($this.attr('type'))
                        _options.type = $this.attr('type');

                    if ($this.attr('icon'))
                        _options.icon = $this.attr('icon');

                    if ($this.attr('dbfield'))
                        _options.dbfield = $this.attr('dbfield');

                    if ($this.attr('caption'))
                        _options.caption = $this.attr('caption');

                    _options.nullable = ($this.attr('nullable') === 'true') || ($this.attr('nullable') === '');

                    _options.notnull = ($this.attr('notnull') === 'true') || ($this.attr('notnull') === '');

                    if ($this.attr('digits'))
                        _options.digits = parseInt($this.attr('digits'));

                    if ($this.attr('maxlength'))
                        _options.maxlength = parseInt($this.attr('maxlength'));

                    if ($this.attr('changetime'))
                        _options.changetime = parseInt($this.attr('changetime'));

                    if ($this.attr('minheight'))
                        _options.minheight = parseInt($this.attr('minheight'));

                    if ($this.attr('maxheight'))
                        _options.maxheight = parseInt($this.attr('maxheight'));

                    if ($this.attr('fixheight'))
                        _options.fixheight = parseInt($this.attr('fixheight'));

                    if ($this.attr('suggest'))
                        _options.suggest = ($this.attr('suggest') === 'true') || ($this.attr('suggest') === '');

                    $this.data(options, $.extend({}, default_edit, _options, options_set));
                    _options = $this.data(options);

                    $this.addClass('lk-editor');

                    if ($this.data(options).icon.length !== 0)
                        $this.append($('<div>').addClass('lk-editor--icon').append($('<i>').addClass('lk-icon ' + $this.data(options).icon)));
                    $this.append($('<div>').addClass('lk-editor--inner'));
                    switch (_options.type) {
                        case 'bool' :
                        {
                            $this.find('.lk-editor--inner').css({
                                width: 'calc(100% - ' + (($this.data(options).icon.length !== 0) ? (parseInt($this.find('.lk-editor--icon .lk-icon').css('width'))) : 0) + 'px)'
                            });
                            $('<span>').addClass('lk-editor--focus').insertBefore($this.find('.lk-editor--inner'));
                            $this.find('.lk-editor--focus').css({
                                left: parseInt($this.find('.lk-editor--focus').css('left')) + (($this.data(options).icon.length !== 0) ? (parseInt($this.find('.lk-editor--icon .lk-icon').css('width'))) : 0)
                            });

                            $this.append($('<span>').addClass('ripple').addClass('circle').css({'background-color': _options.ripplecolor}));
                            $this.find('.ripple').css({
                                left: parseInt($this.find('.ripple').css('left')) + (($this.data(options).icon.length !== 0) ? (parseInt($this.find('.lk-editor--icon .lk-icon').css('width'))) : 0)
                            });

                            $this.find('.lk-editor--inner')
                                    .append($('<div>').addClass('lk-editor--checkbox').attr('tabindex', '0')); // .html(_options.caption));

                            $this.find('.lk-editor--checkbox')
                                    .append($('<i>').addClass('lk-icon icon-cb-true'))
                                    .append($('<i>').addClass('lk-icon icon-cb-false'))
                                    .append($('<i>').addClass('lk-icon icon-cb-null'));
                            if ((_options.nullable === true) || (_options.nullable === 'true')) {
                                _options.boolval = 'null';
                                $this.find('.lk-editor--checkbox').addClass('null');
                            } else {
                                _options.boolval = 'false';
                                $this.find('.lk-editor--checkbox').addClass('false');
                            }
                            break;
                        }
                        case 'string' :
                        {
                            $this.find('.lk-editor--inner').css({
                                width: 'calc(100% - ' + (($this.data(options).icon.length !== 0) ? (parseInt($this.find('.lk-editor--icon .lk-icon').css('width'))) : 0) + 'px)'
                            });
                            $this.find('.lk-editor--inner').append($('<input>').attr('type', 'text').addClass('lk-editor--input').attr('maxlength', _options.maxlength));

                            if (_options.suggest === true) {
                                $this.append($('<div>').addClass('lk-editor--suggest'));
                                $this.find('.lk-editor--suggest').css({
                                    width: 'calc(100% - ' + (($this.data(options).icon.length !== 0) ? (parseInt($this.find('.lk-editor--icon .lk-icon').css('width'))) : 0) + 'px)'
                                });
                            }
                            break;
                        }
                        case 's-dict' :
                        {
                            if ((_options.multiselect === true) || (_options.multiselect === "true")) {

                            } else {
                                _options.linkval = {id: '', text: ''};
                                $this.find('.lk-editor--inner').css({
                                    width: 'calc(100% - ' + ((($this.data(options).icon.length !== 0) ? (parseInt($this.find('.lk-editor--icon .lk-icon').css('width'))) : 0) + EDITOR__BUTTON_WIDTH * 2 + EDITOR__BUTTON_RIGHT * 3 - EDITOR__PADDING_RIGHT) + 'px)'
                                });
                                $this.find('.lk-editor--inner').append($('<input>').attr('type', 'text').addClass('lk-editor--input').attr('readonly', 'readonly'));
                                $this.append($('<div>').addClass('lk-editor--buttons').css({
                                    width: EDITOR__BUTTON_WIDTH * 2 + EDITOR__BUTTON_RIGHT * 3
                                }));
                                $this.find('.lk-editor--buttons')
                                        .append($('<span>').addClass('lk-editor--button btn1').append($('<span>').addClass('lk-icon icon-ok-tick')))
                                        .append($('<span>').addClass('lk-editor--button btn0').append($('<span>').addClass('lk-icon icon-clean')));
                                break;
                            }
                        }
                        case 'integer' :
                        {
                            $this.find('.lk-editor--inner').css({
                                width: 'calc(100% - ' + (($this.data(options).icon.length !== 0) ? (parseInt($this.find('.lk-editor--icon .lk-icon').css('width'))) : 0) + 'px)'
                            });
                            $this.find('.lk-editor--inner').append($('<input>').attr('type', 'text').addClass('lk-editor--input'));
                            break;
                        }
                        case 'float' :
                        {
                            $this.find('.lk-editor--inner').css({
                                width: 'calc(100% - ' + (($this.data(options).icon.length !== 0) ? (parseInt($this.find('.lk-editor--icon .lk-icon').css('width'))) : 0) + 'px)'
                            });
                            $this.find('.lk-editor--inner').append($('<input>').attr('type', 'text').addClass('lk-editor--input'));
                            break;
                        }
                        case 'date' :
                        {
                            $this.find('.lk-editor--inner').css({
                                width: 'calc(100% - ' + ((($this.data(options).icon.length !== 0) ? (parseInt($this.find('.lk-editor--icon .lk-icon').css('width'))) : 0) + EDITOR__BUTTON_WIDTH * 1 + EDITOR__BUTTON_RIGHT * 2 - EDITOR__PADDING_RIGHT) + 'px)'
                            });
                            $this.find('.lk-editor--inner').append($('<input>').attr('type', 'text').addClass('lk-editor--input'));
                            $this.append($('<div>').addClass('lk-editor--buttons').css({
                                width: EDITOR__BUTTON_WIDTH * 1 + EDITOR__BUTTON_RIGHT * 2
                            }));
                            $this.find('.lk-editor--buttons')
                                    .append($('<span>').addClass('lk-editor--button btn0').append($('<span>').addClass('lk-icon icon-date')));
                            break;
                        }
                        case 'datetime' :
                        {
                            $this.find('.lk-editor--inner').css({
                                width: 'calc(100% - ' + ((($this.data(options).icon.length !== 0) ? (parseInt($this.find('.lk-editor--icon .lk-icon').css('width'))) : 0) + EDITOR__BUTTON_WIDTH * 1 + EDITOR__BUTTON_RIGHT * 2 - EDITOR__PADDING_RIGHT) + 'px)'
                            });
                            $this.find('.lk-editor--inner').append($('<input>').attr('type', 'text').addClass('lk-editor--input'));
                            $this.append($('<div>').addClass('lk-editor--buttons').css({
                                width: +EDITOR__BUTTON_WIDTH * 1 + EDITOR__BUTTON_RIGHT * 2
                            }));
                            $this.find('.lk-editor--buttons')
                                    .append($('<span>').addClass('lk-editor--button btn0').append($('<span>').addClass('lk-icon icon-date')));
                            break;
                        }
                        case 'strlong' :
                        {
                            $this.find('.lk-editor--inner').css({
                                width: 'calc(100% - ' + (($this.data(options).icon.length !== 0) ? (parseInt($this.find('.lk-editor--icon .lk-icon').css('width'))) : 0) + 'px)'
                            });
                            $this.find('.lk-editor--inner').append($('<textarea>').addClass('lk-editor--textarea').attr('rows', 1).attr('maxlength', _options.maxlength));

                            if ((_options.minheight) && (parseInt(_options.minheight) > 0))
                                $this.find('.lk-editor--textarea').css({
                                    'min-height': parseInt(_options.minheight)
                                });

                            if ((_options.maxheight) && (parseInt(_options.maxheight) > 0))
                                $this.find('.lk-editor--textarea').css({
                                    'max-height': parseInt(_options.maxheight)
                                });

                            if ((_options.fixheight) && (parseInt(_options.fixheight) > 0))
                                $this.find('.lk-editor--textarea').css({
                                    'min-height': parseInt(_options.fixheight),
                                    'max-height': parseInt(_options.fixheight)
                                });
                            break;
                        }
                        case 'text' :
                        {
                            $this.find('.lk-editor--inner').css({
                                width: 'calc(100% - ' + (($this.data(options).icon.length !== 0) ? (parseInt($this.find('.lk-editor--icon .lk-icon').css('width'))) : 0) + 'px)'
                            });
                            $this.find('.lk-editor--inner').append($('<textarea>').addClass('lk-editor--textarea').attr('rows', 1));
                            if ((_options.minheight) && (parseInt(_options.minheight) > 0))
                                $this.find('.lk-editor--textarea').css({
                                    'min-height': parseInt(_options.minheight)
                                });

                            if ((_options.maxheight) && (parseInt(_options.maxheight) > 0))
                                $this.find('.lk-editor--textarea').css({
                                    'max-height': parseInt(_options.maxheight)
                                });

                            if ((_options.fixheight) && (parseInt(_options.fixheight) > 0))
                                $this.find('.lk-editor--textarea').css({
                                    'min-height': parseInt(_options.fixheight),
                                    'max-height': parseInt(_options.fixheight)
                                });
                            break;
                        }
                    }

                    $this.find('.lk-editor--inner')
                            .append($('<span>').addClass('lk-editor--label').html(_options.caption))
                            .append($('<hr>').addClass('lk-editor--footer'))
                            .append($('<span>').addClass('lk-editor--info'));

                    if (_options.type === 'bool') {
//                        $this.find('.lk-editor--label').html('');
                        $this.find('.lk-editor--label').addClass('lk-editor--label-cb').removeClass('lk-editor--label');
                    }

                    _options._canblur = true;
                    $this.find('.lk-editor--input, .lk-editor--textarea, .lk-editor--link').on({
                        focus: __input__focus,
                        blur: __input__blur,
                        'input propertychange change': __input__input
                    });
                    if (_options.type !== 'bool') {
                        $this.find('.lk-editor--label').on({
                            click: __label__click
                        });
                    } else {
                        $this.find('.lk-editor--label-cb, .lk-editor--checkbox .lk-icon').on({
                            mousedown: __bool__mousedown
                        });

                        $this.find('.lk-editor--checkbox').on({
                            mousedown: __bool__mousedown,
                            focus: __bool__focus,
                            blur: __bool__blur,
                            keydown: __bool_keydown
                        });
                    }
                    $this.find('.lk-editor--button').on({
                        click: __btn__click,
                        mousedown: __btn__mousedown
                    });

                    if ((_options.type === 'string') && (_options.suggest === true)) {
                        $this.find('.lk-editor--input').on('keydown', __input__keydown);
                        $this.find('.lk-editor--suggest').on({mouseover: __suggest__mouse_over, mouseout: __suggest__mouse_out, click: __suggest__click});
                    }

                    if ($this.hasClass('disabled') || $this.attr('disabled'))
                        methods.disable.call($this);
                    $this.find('.lk-editor--input, .lk-editor--textarea').change();
                }
            });
        },

        destroy: function () {
            return this.each(function () {
                var $this = $(this);
                if ($this.data(options)) {
                    $this.find('*').off().remove();
                    $this.removeData();
                }
            });
        },

        enable: function () {
            return this.each(function () {
                var $this = $(this);
                if (!$this.data(options))
                    return;
                $this.find('.lk-editor--input').removeAttr('disabled');
                $this.find('.lk-editor--textarea').removeAttr('disabled');
                $this.find('.lk-editor--checkbox').attr('tabindex', '0');
                $this.removeClass('disabled');
            });
        },

        disable: function () {
            return this.each(function () {
                var $this = $(this);
                if (!$this.data(options))
                    return;
                $this.find('.lk-editor--input').attr('disabled', 'disabled');
                $this.find('.lk-editor--textarea').attr('disabled', 'disabled');
                $this.find('.lk-editor--checkbox').removeAttr('tabindex');
                $this.addClass('disabled');
            });
        },

        dbField: function () {
            var $this = $(this), _options = $this.data(options);
            return _options.dbfield;
        },

        dbValue: function () {
            var $this = $(this), _options = $this.data(options);

            if (arguments.length === 0) {

                if (['string', 'date', 'datetime'].indexOf(_options.type) !== -1) {
                    return $this.find('.lk-editor--input').val();
                }

                if (['text', 'strlong'].indexOf(_options.type) !== -1) {
                    return $this.find('.lk-editor--textarea').val();
                }

                if (_options.type === 'bool') {
                    return {'true-val': 'Y', 'false-val': 'N', 'null-val': ''}[_options.boolval + '-val'];
                }

                if (_options.type === 's-dict') {
                    return _options.linkval;
                }

                if (['integer', 'float'].indexOf(_options.type) !== -1) {
                    return $this.find('.lk-editor--input').val().trim().replace(/[,]/g, '.');
                }
            }

            if (arguments.length !== 0) {

                if (['string', 'date', 'datetime'].indexOf(_options.type) !== -1) {
                    $this.find('.lk-editor--input').val(arguments[0]);
                    $this.find('.lk-editor--input').trigger('blur');
                }

                if (['text', 'strlong'].indexOf(_options.type) !== -1) {
                    $this.find('.lk-editor--textarea').val(arguments[0]);
                    $this.find('.lk-editor--textarea').trigger('blur');
                }

                if (_options.type === 's-dict') {
                    _options.linkval = arguments[0];
                    $this.find('.lk-editor--input').val(_options.linkval.text);
                }

                if (_options.type === 'bool') {
                    _options.boolval = {'Y-val': 'true', 'N-val': 'false', '-val': 'null'}[arguments[0] + '-val'];
                    $this.find('.lk-editor--checkbox').removeClass('true').removeClass('false').removeClass('null').addClass(_options.boolval);
                    if (_options.boolval === 'null') {
                        $this.removeClass('value');
                    } else {
                        $this.addClass('value');
                    }
                }
            }
        },

        validate: function (_setValue) {
            return this.each(function () {
                var $this = $(this), _options = $this.data(options), _val, _pval, _fmt;
                if ('integer,float,date,datetime'.split(',').indexOf(_options.type) === -1)
                    return;
                _val = $this.find('.lk-editor--input').val().trim();
                $this.removeClass('error');
                $this.find('.lk-editor--info').html('');
                _fmt = DISPLAY_FORMATS[_options.type];

                switch (_options.type) {
                    case 'integer' :
                    {
                        _val = _val.replace(/[.]/g, ',');
                        _pval = numeral(_val);
                        if ((_val === '') && (_options.notnull))
                            $this.addClass('error');
                        if ((_val !== '') && (_pval._value === null))
                            $this.addClass('error');
                        break;
                    }
                    case 'float' :
                    {
                        _val = _val.replace(/[.]/g, ',');
                        _pval = numeral(_val);
                        if ((_val === '') && (_options.notnull))
                            $this.addClass('error');
                        if ((_val !== '') && (_pval._value === null))
                            $this.addClass('error');
                        _fmt = DISPLAY_FORMATS['float'] + '00000000'.substring(0, _options.digits);
                        break;
                    }
                    case 'date' :
                    {
                        _pval = moment(_val, [PARSE_FORMATS.date]);
                        if ((_val === '') && (_options.notnull))
                            $this.addClass('error');
                        if ((_val !== '') && (_pval._isValid === false))
                            $this.addClass('error');
                        break;
                    }
                    case 'datetime' :
                    {
                        _pval = moment(_val, [PARSE_FORMATS.datetime]);
                        if ((_val === '') && (_options.notnull))
                            $this.addClass('error');
                        if ((_val !== '') && (_pval._isValid === false))
                            $this.addClass('error');
                        break;
                    }
                }

                if ($this.hasClass('error'))
                    if ((_val === ''))
                        $this.find('.lk-editor--info').html(ERROR_MESSAGES.notnull);
                    else
                        $this.find('.lk-editor--info').html(ERROR_MESSAGES[_options.type]);
                else
                if ((_setValue === true) && (_val !== ''))
                    $this.find('.lk-editor--input').val(_pval.format(_fmt));
            });
        },
        suggclose: function () {
            var $this = $(this), _options = $this.data(options),
                    $sugg = $this.find('.lk-editor--suggest');
            if ($sugg.length === 0) {
                return;
            }
            $sugg.find('*').off().remove();
            $sugg.css({display: 'none'});
            _options.priorRequest = '915a0e20-cf19-40b3-8f49-05975440f7aa';
            _options._priorValue = 'f3bfdafb-3445-4400-8f76-ae51dd9bff26';
        }
    };

    $.fn.lkEditor = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else {
            if (typeof method === 'object' || !method) {
                return methods.create.apply(this, arguments);
            } else {
                $.error('lkEditor: method ' + method + ' not found');
            }
        }
    };

    function _bool_modify(_this) {
        var $this = $(_this), _options = $this.data(options), $ed = $this.find('.lk-editor--checkbox'), _val = methods.dbValue.call($this);
        if ((_options.nullable) || (_options.nullable === 'true')) {
            if (_val === '') {
                methods.dbValue.call($this, 'Y');
                return;
            }
            if (_val === 'Y') {
                methods.dbValue.call($this, 'N');
                return;
            }
            methods.dbValue.call($this, '');
            return;
        } else {
            if (_val === 'N') {
                methods.dbValue.call($this, 'Y');
                return;
            }
            methods.dbValue.call($this, 'N');
            return;
        }
    }

    function __bool_keydown(event) {
        if (event.keyCode !== 32)
            return;
        var $this = $(event.target).closest('.lk-editor'), _options = $this.data(options), ripple = $this.find('.ripple');
        if ($this.hasClass('disabled'))
            return;
        if ((_options.ripple === true) || (_options.ripple === 'true'))
            ripple.removeClass('animate').css({'width': ripple.css('width')}).addClass('animate');
        _bool_modify($this);
    }

    function __bool__blur(event) {
        var $this = $(event.target).closest('.lk-editor'), _options = $this.data(options);
        $(event.target).closest('.lk-editor').removeClass('active');
    }

    function __bool__focus(event) {
        var $this = $(event.target).closest('.lk-editor'), _options = $this.data(options);
        $(event.target).closest('.lk-editor').addClass('active');
    }

    function __bool__mousedown(event) {
        event.preventDefault();
        if (event.button !== 0)
            return;
        var $this = $(event.target).closest('.lk-editor'), _options = $this.data(options), ripple = $this.find('.ripple');
        if ($this.hasClass('disabled'))
            return;
        if (((_options.ripple === true) || (_options.ripple === 'true')) && (GLOBAL_RIPPLE))
            ripple.removeClass('animate').css({'width': ripple.css('width')}).addClass('animate');
        _bool_modify($this);
        $this.find('.lk-editor--checkbox').focus();
    }

    function __input__focus() {
        var $this = $(this).closest('.lk-editor'), _options = $this.data(options);
        $this.addClass('active');
    }

    function __input__blur() {
        var $this = $(this).closest('.lk-editor'), _options = $this.data(options);
        if (_options._canblur)
            $this.removeClass('active');
        _options._canblur = true;
        setTimeout(function () {
            methods['suggclose'].apply($this);
        }, _BLUR_TIMEOUT);

        if ($this.find('.lk-editor--input').length > 0) {
            if ($this.find('.lk-editor--input').val().length === 0) {
                $this.removeClass('value');
            } else {
                ($this.addClass('value'));
            }
        }

        if ($this.find('.lk-editor--textarea').length > 0) {
            if ($this.find('.lk-editor--textarea').val().length === 0) {
                $this.removeClass('value');
            } else {
                ($this.addClass('value'));
            }
        }
        methods['validate'].apply($this, [true]);
    }

    function __input__input(event) {
        var $this = $(event.target).closest('.lk-editor'), _options = $this.data(options), _method, _editClass;

        clearTimeout(_options._delaying);

        if ($this.find('.lk-editor--textarea').length > 0) {
            _editClass = '.lk-editor--textarea';
        } else {
            _editClass = '.lk-editor--input';
        }

        if (_editClass === '.lk-editor--textarea') {
            $this.find(_editClass).css({'height': 'auto'});
            $this.find(_editClass).css({'height': $this.find(_editClass).get(0).scrollHeight});
        }

        if ((_options.maxlength) && (['string', 'strlong'].indexOf(_options.type) !== -1))
            $this.find('.lk-editor--info').css({
                'text-align': 'right'
            }).html($this.find(_editClass).val().length + '/' + $this.find(_editClass).attr('maxlength'));

        if ($this.find(_editClass).val().length === 0) {
            $this.removeClass('value');
        } else {
            ($this.addClass('value'));
        }
        methods['validate'].apply($this);
        var currValue = methods['dbValue'].apply($this);
        if (_options._priorValue !== currValue) {

//            console.log(_options._priorValue, ' => ', currValue);

            if ((!$this.hasClass('error')) && (event.type !== 'change')) {
                _method = _options.onChange;
                if (_method) {
                    _options._delaying = setTimeout(function () {
                        _method.apply($this);
                    }, _options.changetime);
                }
            }
            if (_options.suggest === true) {
                _method = _options.onSuggest;
                if (_method) {
                    _method.apply($this);
                }
            }
            _options._priorValue = currValue;
        }
    }

    function __label__click() {
        var $this = $(this).closest('.lk-editor');
        $this.find('.lk-editor--input, .lk-editor--textarea').trigger('focus');
    }

    function __btn__click() {
        var $this = $(this).closest('.lk-editor'), _options = $this.data(options), _method, $btn = $($(this).closest('.lk-editor--button').get(0));

        if ($this.hasClass('disabled')) {
            return;
        }
        $this.addClass('active');

        switch (_options.type) {
            case 'date':
            {
                _method = _options.selectClick;
                if (_method) {
                    _method.call($this, $btn);
                } else {
                    $.lkDatePicker({
                        style: 'date',
                        control: $this
                    });
                }
                break;
            }
            case 'datetime':
            {
                _method = _options.selectClick;
                if (_method) {
                    _method.call($this, $btn);
                } else {
                    $.lkDatePicker({
                        style: 'datetime',
                        control: $this
                    });
                }
                break;
            }
            case 's-dict' :
            {
                if ($btn.hasClass('btn0')) {
                    _method = _options.cancelClick;
                    if (_method) {
                        _method.call($this, $btn);
                    } else {
                        console.log(methods['dbValue'].apply($this));
                        methods['dbValue'].apply($this, [{id: '', text: ''}]);
                    }
                } else {
                    _method = _options.selectClick;
                    if (_method) {
                        _method.call($this, $btn);
                    } else {
                        methods['dbValue'].apply($this, [{id: 10, text: 'ok'}]);
                        console.log('default select button click link field behavior');
                    }
                }
                $this.find('.lk-editor--input').trigger('focus');
            }
        }
        _options._canblur = true;
    }

    function __btn__mousedown() {
        var $this = $(this).closest('.lk-editor'), _options = $this.data(options);
        _options._canblur = false;
    }

    function __input__keydown(event) {
        var $this = $(event.target).closest('.lk-editor'), _options = $this.data(options), _method;
        if (_options.suggest !== true) {
            return;
        }
        var $el = $this.find('.lk-suggest--item');
        if ($el.length === 0) {
            return;
        }
        var $e = $this.find('.lk-suggest--item.active');
        if ($e.length === 0) {
            switch (event.keyCode) {
                case 38 :
                {
                    $($el.get($el.length - 1)).addClass('active');
                    event.preventDefault();
                    break;
                }
                case 40 :
                {
                    $($el.get(0)).addClass('active');
                    event.preventDefault();
                    break;
                }
                case 13 :
                {
                    methods['suggclose'].apply($this);
                    $this.find('.lk-editor--input').trigger('focus');
                    break;
                }
            }
        } else {
            var idx = $el.index($e);
            switch (event.keyCode) {
                case 38 :
                {
                    idx = (idx === 0) ? ($el.length - 1) : (idx - 1);
                    event.preventDefault();
                    break;
                }
                case 40 :
                {
                    idx = (idx === $el.length - 1) ? 0 : (idx + 1);
                    event.preventDefault();
                    break;
                }
                case 13 :
                {
                    event.preventDefault();
                    _method = _options.onSuggested;
                    if (_method) {
                        _method.apply($this);
                    }
                    methods['suggclose'].apply($this);
                    $this.find('.lk-editor--input').trigger('focus');
                    break;
                }
            }
            $el.removeClass('active');
            $($el.get(idx)).addClass('active');
        }
    }

    function __suggest__mouse_over(event) {
        var $this = $(this).closest('.lk-editor'), _options = $this.data(options);
        var $e = $this.find('.lk-suggest--item:hover');
        if ($e.length !== 0) {
            $this.find('.lk-suggest--item').removeClass('active');
            $e.addClass('active');
        }
        _options._canblur = false;
    }

    function __suggest__mouse_out(event) {
        var $this = $(this).closest('.lk-editor'), _options = $this.data(options);
        _options._canblur = true;
    }

    function __suggest__click(event) {
        var $this = $(this).closest('.lk-editor'), _options = $this.data(options),
                _method = _options.onSuggested;
        if (_method) {
            _method.apply($this);
        }
        methods['suggclose'].apply($this);
        $this.find('.lk-editor--input').trigger('focus');
    }

})(jQuery);