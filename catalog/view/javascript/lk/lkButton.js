;
(function ($) {
    /*
     * @type            bytton type ( raised, flat, fab, fab-mini, icon )
     * @icon            button icon class
     * @iconRotate      icon rotation degree ( 45, 90, 135, 180, 225, 270, 315 )
     * @backgroundColor dbfield name 
     * @captionColor
     * @rippleColor
     * @ripple          ripple effect ( true, false )
     * @repeat          repeat pressed button ( default = false )
     * @firstRepeat     first click timeout, ms ( default = 400 )
     * @nextRepeat      2'nd and other repeat timeout, ms ( default = 50 )
     * 
     * @onClick         function(this)
     */
    var options = 'lkButton';
    var methods = {

        create: function (options_set) {
            var defaults = {
                type: 'raised',
                backgroundColor: '',
                captionColor: '',
                rippleColor: '',
                ripple: true,
                icon: 'home',
                iconRotate: 0,
                iconPosition: '',
                repeat: false,
                firstRepeat: 400,
                nextRepeat: 50
// onClick: function(this, event)
            };
            return this.each(function () {
                var $this = $(this), _options = {};
                if (!($this.data(options))) {
                    if (!$this.is('button'))
                        $.error('lkButton: create - element is not button');

                    if ($this.attr('type'))
                        _options.type = $this.attr('type');

                    if ($this.attr('backgroundColor'))
                        _options.backgroundColor = $this.attr('backgroundColor');

                    if ($this.attr('captionColor'))
                        _options.captionColor = $this.attr('captionColor');

                    if ($this.attr('rippleColor'))
                        _options.rippleColor = $this.attr('rippleColor');

                    if ($this.attr('ripple'))
                        _options.ripple = $this.attr('ripple');

                    if ($this.attr('icon'))
                        _options.icon = $this.attr('icon');

                    if ($this.attr('iconRotate'))
                        _options.iconRotate = $this.attr('iconRotate');

                    if ($this.attr('iconPosition'))
                        _options.iconPosition = $this.attr('iconPosition');

                    if ($this.attr('repeat'))
                        _options.repeat = $this.attr('repeat');

                    if ($this.attr('firstRepeat'))
                        _options.firstRepeat = $this.attr('firstRepeat');

                    if ($this.attr('nextRepeat'))
                        _options.nextRepeat = $this.attr('nextRepeat');

                    $this.data(options, $.extend({}, defaults, _options, options_set));
                    _options = $this.data(options);
                    switch (_options.type) {
                        case 'raised':
                        {
                            $this.addClass('lk-button').addClass('lk-button--raised').css({
                                'background-color': _options.backgroundColor,
                                'color': _options.captionColor
                            }).append($('<span>').addClass('ripple-frame').append($('<span>').addClass('ripple').css({
                                'background-color': _options.rippleColor
                            })));
                            if (_options.iconPosition === 'left') {
                                $this.addClass('lk-button-icon-left');
                            }
                            if (_options.iconPosition === 'right') {
                                $this.addClass('lk-button-icon-right');
                            }
                            break;
                        }
                        case 'flat':
                        {
                            $this.addClass('lk-button').addClass('lk-button--flat').css({
                                'background-color': _options.backgroundColor,
                                'color': _options.captionColor
                            }).append($('<span>').addClass('ripple-frame').append($('<span>').addClass('ripple').css({
                                'background-color': _options.rippleColor
                            })));
                            break;
                            if (_options.iconPosition === 'left') {
                                $this.addClass('lk-button-icon-left');
                            }
                            if (_options.iconPosition === 'right') {
                                $this.addClass('lk-button-icon-right');
                            }
                        }
                        case 'fab':
                        {
                            _r = parseInt((parseInt('0' + _options.iconRotate) || 0) / 45) * 45;
                            $this.addClass('lk-button').addClass('lk-button--fab').css({
                                'background-color': _options.backgroundColor,
                                'color': _options.captionColor
                            }).html('').append($('<i>').addClass('lk-icon ' + _options.icon)).append($('<span>').addClass('ripple-frame').addClass('circle').append($('<span>').addClass('ripple').css({
                                'background-color': _options.rippleColor
                            })));
                            break;
                        }
                        case 'fab-mini':
                        {
                            _r = parseInt((parseInt('0' + _options.iconRotate) || 0) / 45) * 45;
                            $this.addClass('lk-button').addClass('lk-button--fab-mini').css({
                                'background-color': _options.backgroundColor,
                                'color': _options.captionColor
                            }).html('').append($('<i>').addClass('lk-icon ' + _options.icon)).append($('<span>').addClass('ripple-frame').addClass('circle').append($('<span>').addClass('ripple').css({
                                'background-color': _options.rippleColor
                            })));
                            break;
                        }
                        case 'icon':
                        {
                            _r = parseInt((parseInt('0' + _options.iconRotate) || 0) / 45) * 45;
                            $this.addClass('lk-button').addClass('lk-button--icon').css({
                                'background-color': _options.backgroundColor,
                                'color': _options.captionColor
                            }).html('').append($('<i>').addClass('lk-icon ' + _options.icon)).addClass('ripple-frame').addClass('circle').append($('<span>').addClass('ripple').css({
                                'background-color': _options.rippleColor
                            }));
                            break;
                        }
                        default :
                        {
                            $.error('lkButton: button type ' + _options.type + ' not found');
                            break;
                        }
                    }

                    $this.find('span').hover(
                            function () {
                                var $this = $(this).closest('button');
                                if (!$this.attr('disabled'))
                                    $this.addClass('hover');
                            },
                            function () {
                                var $this = $(this).closest('button');
                                $this.removeClass('hover');
                            }
                    );

                    $this.hover(
                            function () {
                                var $this = $(this).closest('button');
                                if (!$this.attr('disabled'))
                                    $this.addClass('hover');
                            },
                            function () {
                                var $this = $(this).closest('button');
                                $this.removeClass('hover');
                            }
                    );

                    $this.on({
                        mouseup: __mouseup,
//                        mouseleave: __mouseup,
                        mousedown: __mousedown__for_repeat,
                        keydown: __keydown,
                        keyup: __keyup,
                        click: _click
                    });
                }
            });
        },

        enable: function () {
            return this.each(function () {
                var $this = $(this);
                if ($this.data(options))
                    $this.removeAttr('disabled');
            });
        },

        disable: function () {
            return this.each(function () {
                var $this = $(this);
                if ($this.data(options))
                    $this.attr('disabled', 'disabled');
            });
        },

        destroy: function () {
            return this.each(function () {
                var $this = $(this);
                if ($this.data(options)) {
                    $this.find('*').off();
                    $this.remove();
                }
            });
        }
    };

    $.fn.lkButton = function (method) {
        if (methods[method])
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        else
        if (typeof method === 'object' || !method)
            return methods.create.apply(this, arguments);
        else
            $.error('lkButton: method ' + method + ' not found');
    };

    function __btn__repeat(_this) {
        var $this = $(_this), _options = $this.data(options);
        $this.click();
        _options.rt = setTimeout(function () {
            __btn__repeat($this);
        }, _options.nextRepeat);
    }

    function __mousedown__for_repeat(event) {
        var $this = $(this).closest('button'),
                _options = $this.data(options);

        if (event.button !== 0)
            return;
        if ($this.attr('disabled'))
            return true;

        if ((_options.ripple === 'true') || (_options.ripple === true))
            if (GLOBAL_RIPPLE) {
                var d, x, y, ripple = $this.find('.ripple');
                ripple.removeClass('animate');
                d = Math.max($this.outerWidth(), $this.outerHeight());
                x = event.pageX - $this.offset().left - d / 2;
                y = event.pageY - $this.offset().top - d / 2;
                ripple.css({
                    top: y,
                    left: x,
                    height: d,
                    width: d
                }).addClass('animate');
            }

        event.stopPropagation();
        if (((_options.repeat === true) || (_options.repeat === 'true')) && (!$this.hasClass('pressed'))) {
            $this.addClass('pressed');
            $(document).on({
                'mouseup mouseleave': __document__mouseup
            });
            _options.rt = setTimeout(function () {
                __btn__repeat($this);
            }, _options.firstRepeat);
        }
    }

    function __mouseup() {
        var $this = $(this).closest('button');
        $this.trigger('blur');
        $this.removeClass('active');
    }

    function __document__mouseup() {
        var $this = $('button.pressed'), _options = $this.data(options);
        if ($this.length > 0) {
            clearTimeout(_options.rt);
            $(document).off('mouseup').off('mouseleave');
        }
        $this.removeClass('pressed');
    }

    function __keydown(event) {
        var $this = $(this);
        if ((event.keyCode === 13) || (event.keyCode === 32)) {
            if (event.keyCode === 32) {
                $this.addClass('pressed');
            }
            $this.addClass('active');
        }
    }

    function __keyup(event) {
        var $this = $(event.target).closest('button'), _options = $this.data(options);
        $this.removeClass('active');
    }

    function _click(event) {
        var $this = $(event.target).closest('button'),
                _options = $this.data(options),
                _m = _options.onClick;
        if (_m)
            _m.call($this, event);
    }

})(jQuery);