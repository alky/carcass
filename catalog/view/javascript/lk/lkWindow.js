;(function ($) {
    var options = 'lkWindow';
    var _RIPPLE_TIME = 75;
    var _RIPPLE_TIME_CLOSE = 150;
    var _WINDOW_ANIMATION = 250;

    /*
     * @onResize(this, page)
     * 
     */    
    
    var methods = {

        create: function (options_set) {
            var defaults = {
                formicon: '',
                title: '',
                modal: false,
                height: 300,
                width: 500,
                top: 150,
                left: 1000,
                minWidth: 350,
                minHeight: 250,
                move: false,
                resize: false,
                useblur: true,
                sysbuttons: {}, // minimize: { click: function () {} },maximize: { click: function () {} },close: { click: function () {} }
                buttons: [] // { caption: '', click: function () {} }
            };
            return this.each(function () {
                if (!($(this).data('obj'))) {
                    $(this).data('obj', true);
                    $(this).data(options, $.extend({}, defaults, options_set));
                    var $this = $(this), _options = $this.data(options);
                    _options.id = lk.getGUID();
                    _options.taskbarid = lk.getGUID();
                }
            });
        },

        show: function () {
            return this.each(function () {
                var $this = $(this), _options = $this.data(options), $wnd = $this.closest('.lk-window'), $taskBar = $('.lk-taskbar'), $taskBar_item, _els, _btn;
                if (_options.modal)
                    lk.setBkg(_options.id, _options.useblur);
                if ($wnd.length === 0) {
                    $('.lk-window').removeClass('lk-window-active');
                    $wnd = $('<div>').attr('id', _options.id).attr('tabindex', 0).css({
                        'width': _options.width,
                        'height': _options.height,
                        'left': _options.left,
                        'top': _options.top,
                        'min-width': _options.minWidth,
                        'min-height': _options.minHeight,
                        'max-width': _options.maxWidth,
                        'max-height': _options.maxHeight
                    }).addClass('lk-window').addClass('lk-window-active').appendTo($('body'));
                    if (_options.title.trim() !== '') {
                        $('<div>').addClass('lk-window--header').addClass('unselectable').html(_options.title).appendTo($wnd);
                        if (_options.formicon !== '') {
                            $wnd.find('.lk-window--header').append($('<span>').addClass('lk-window--header-icon').addClass(_options.formicon));
                            $wnd.find('.lk-window--header').css({
                                'padding-left': parseInt($wnd.find('.lk-window--header').css('padding-left')) * 3 / 2 + $wnd.find('.lk-window--header .lk-window--header-icon').outerWidth()
                            });
                        }
                        var _btns = _options.sysbuttons;
                        if (_btns.minimize || _btns.maximize || _btns.close) {
                            $('<div>').addClass('lk-window--system-buttons').appendTo($wnd.find('.lk-window--header'));

                            if ((_btns.minimize) && !((_options.modal === true) || (_options.modal === 'true')))
                            {
                                _btn = $('<button>').addClass('lk-window--btn-min').lkButton({type: 'icon', ripple: false, icon: 'icon-arrow-down'});
                                $wnd.find('.lk-window--system-buttons').append(_btn);
                                _btn.data('callback', _btns.minimize.onClick);
                                _btn.on({
                                    click: function (event) {
//                                        setTimeout(function () {
                                        var _cc = true, _r = $(event.target).closest('button').data('callback'), $this = $(event.target).closest('.lk-window').find('.lk-window--body');
                                        if (_r)
                                            _cc = _r.call(this, event);
                                        if (!(_cc === false))
                                            methods['minimize'].apply($this);
//                                        }, _RIPPLE_TIME);
                                    }
                                });
                            }

                            if ((_btns.maximize) && !((_options.modal === true) || (_options.modal === 'true'))) {
                                _btn = $('<button>').addClass('lk-window--btn-max').lkButton({type: 'icon', ripple: false, icon: 'icon-arrow-up'});
                                $wnd.find('.lk-window--system-buttons').append(_btn);
                                _btn.data('callback', _btns.maximize.onClick);
                                _btn.on({
                                    click: function (event) {
//                                        setTimeout(function () {
                                        $(event.target).closest('button').lkButton('disable');
                                        var _cc = true, _r = $(event.target).closest('button').data('callback'), $this = $(event.target).closest('.lk-window').find('.lk-window--body');
                                        if (_r)
                                            _cc = _r.call(this, event);
                                        if (!(_cc === false))
                                            methods['maximize'].apply($this);
//                                        }, _RIPPLE_TIME);
                                    }
                                });
                            }

                            if (_btns.close) {
                                _btn = $('<button>').addClass('lk-window--btn-close').lkButton({type: 'icon', icon: 'icon-close'});
                                $wnd.find('.lk-window--system-buttons').append(_btn);
                                _btn.data('callback', _btns.close.onClick);
                                _btn.on({
                                    click: function (event) {
                                        setTimeout(function () {
                                            var _cc = true, _r = $(event.target).closest('button').data('callback'), $this = $(event.target).closest('.lk-window').find('.lk-window--body');
                                            if (_r)
                                                _cc = _r.call(this, event);
                                            if (!(_cc === false))
                                                methods['close'].apply($this);
                                        }, _RIPPLE_TIME_CLOSE);
                                    }
                                });
                            }
                        }
                    }
                    if (_options.buttons.length !== 0) {
                        $('<div>').addClass('lk-window--footer').appendTo($wnd);
                        _options.buttons.forEach(function (el) {
                            var _btn = $('<button>').html(el.caption).lkButton({type: 'flat'});
                            _btn.data('callback', el.onClick);
                            $wnd.find('.lk-window--footer').append(_btn);
                            _btn.on({
                                click: function (event) {
                                    setTimeout(function () {
                                        var _r = $(event.target).closest('button').data('callback');
                                        if (_r)
                                            _r.call(this, event);
                                    }, _RIPPLE_TIME);
                                }
                            });
                        });
                    }
                    $wnd.on({
                        'mousedown focusin': __window__focusin
                    });

                    $(this).removeAttr('css style class').addClass('lk-window--body').appendTo($wnd);
                    $(this).lkResizable();
                    _fixbody($wnd);
                    if (_options.resize)
                        $wnd.append($('<div>').addClass('border-left')).append($('<div>').addClass('border-right')).append($('<div>').addClass('border-top')).append($('<div>').addClass('border-bottom')).append($('<div>').addClass('border-left' + ' ' + 'border-top')).append($('<div>').addClass('border-right' + ' ' + 'border-top')).append($('<div>').addClass('border-left' + ' ' + 'border-bottom')).append($('<div>').addClass('border-right' + ' ' + 'border-bottom'));
                    _els = (_options.move || _options.resize) ? (['.lk-window--header', '.border-left', '.border-right', '.border-top', '.border-bottom'].join()) : '';
                    if (_els !== '')
                        $wnd.find(_els).on({
                            mousedown: __window__mousedown
                        });
                } else {
                    $wnd.focus();
                }

                if (($taskBar.length !== 0) && (!_options.modal)) {
                    $taskBar_item = $('#' + _options.taskbarid);
                    if ($taskBar_item.length === 0) {
                        $taskBar_item = $('<div>').addClass('item unselectable').attr('id', _options.taskbarid);
                        $taskBar.append($taskBar_item);
                        $taskBar_item.data(options, {formid: _options.id});

                        if (_options.formicon !== '') {
                            $taskBar_item.append($('<span>').addClass('icon').addClass(_options.formicon));
                        }
                        $taskBar_item.append($('<span>').addClass('caption').html(_options.title));

                    }
                    $taskBar_item.on({
                        click: __taskbar__click
                    });
                }

                _options.windowState = 'wsNormal';
                $wnd.focus();
                $wnd.removeAttr('tabindex');
            });
        },

        center: function () {
            return this.each(function () {
                var $this = $(this), _options = $this.data(options), $wnd = $this.closest('.lk-window');
                if ((_options.windowState === 'wsMaximized') || (_options.windowState === 'wsMinimized'))
                    return;
                _options.left = (parseInt($(window).width()) - _options.width) / 2;
                _options.top = (parseInt($(window).height()) - _options.height) / 2;
                $wnd.css({
                    'left': _options.left + 'px',
                    'top': _options.top + 'px'
                });
            });
        },

        minimize: function () {
            return this.each(function () {
                var $this = $(this), _options = $this.data(options), $wnd = $this.closest('.lk-window'), $taskBar_item = $('#' + _options.taskbarid), _mto;
                if (_options.windowState !== 'wsMinimized') {
                    $('body').append($('<div>').addClass('lk-window--frame').css({
                        left: $wnd.offset().left,
                        top: $wnd.offset().top,
                        width: $wnd.outerWidth(),
                        height: $wnd.outerHeight()
                    }));
                    $('.lk-window--frame').animate({
                        left: parseInt($taskBar_item.offset().left),
                        top: parseInt($taskBar_item.offset().top),
                        width: parseInt($taskBar_item.width()),
                        height: parseInt($taskBar_item.height())
                    }, _WINDOW_ANIMATION * GLOBAL_ANIMATE, function () {
                        $('.lk-window--frame').remove();
                        if (_options.windowState === 'wsNormal') {
                            _options.restoreSize = {
                                left: $wnd.offset().left,
                                top: $wnd.offset().top,
                                width: $wnd.outerWidth(),
                                height: $wnd.outerHeight()
                            };
                        }
                        _options.priorState = _options.windowState;
                        _options.windowState = 'wsMinimized';
                        $wnd.addClass('minimized');
                        $wnd.find('.animate').removeClass('animate');
                    });
                } else {
                    $('body').append($('<div>').addClass('lk-window--frame').css({
                        left: parseInt($taskBar_item.offset().left),
                        top: parseInt($taskBar_item.offset().top),
                        width: parseInt($taskBar_item.width()),
                        height: parseInt($taskBar_item.height())
                    }));
                    if (_options.priorState === 'wsMaximized') {
                        _mto = {left: 0, right: 0, top: 0, bottom: 0};
                        if ($('.lk-application--header').length !== 0) {
                            _mto.left = parseInt($('.lk-application--header').css('padding-left'));
                            _mto.right = parseInt($('.lk-application--header').css('padding-right'));
                            _mto.top = $('.lk-application--header').outerHeight();
                        }
                        if ($('.lk-application--footer').length !== 0) {
                            _mto.bottom = $('.lk-application--footer').outerHeight();
                        }
                        $('.lk-window--frame').animate(_mto, _WINDOW_ANIMATION * GLOBAL_ANIMATE, function () {
                            $wnd.css({width: '', height: ''}).css(_mto);
                            $('.lk-window--frame').remove();
                            _options.windowState = 'wsMaximized';
                            $wnd.removeClass('minimized');
                        });
                    } else {
                        $('.lk-window--frame').animate(_options.restoreSize, _WINDOW_ANIMATION * GLOBAL_ANIMATE, function () {
                            $wnd.css(_options.restoreSize);
                            $('.lk-window--frame').remove();
                            _options.windowState = 'wsNormal';
                            $wnd.removeClass('minimized');
                        });
                    }
                }
            });
        },

        maximize: function () {
            return this.each(function () {
                var $this = $(this), _options = $this.data(options), $wnd = $this.closest('.lk-window'), $btn = $wnd.find('.lk-window--system-buttons').find('.lk-window--btn-max'), _mto;

                if (_options.windowState === 'wsMaximized') {
                    $('body').append($('<div>').addClass('lk-window--frame').css({
                        left: $wnd.offset().left,
                        top: $wnd.offset().top,
                        width: $wnd.outerWidth(),
                        height: $wnd.outerHeight()
                    }));

                    $('.lk-window--frame').animate(_options.restoreSize, _WINDOW_ANIMATION * GLOBAL_ANIMATE, function () {
                        $wnd.css(_options.restoreSize);
                        $('.lk-window--frame').remove();
                        _fixbody($wnd);
                    });

                    _options.windowState = 'wsNormal';
                    $btn.find('.lk-icon').attr('class', 'lk-icon icon-arrow-up');
                    $btn.lkButton('enable');
                    $wnd.removeClass('maximized');
                    
                } else {
                    _options.restoreSize = {
                        left: $wnd.offset().left,
                        top: $wnd.offset().top,
                        width: $wnd.outerWidth(),
                        height: $wnd.outerHeight()
                    };

                    $('body').append($('<div>').addClass('lk-window--frame').css({
                        left: $wnd.offset().left,
                        top: $wnd.offset().top,
                        right: $(window).width() - $wnd.offset().left - $wnd.outerWidth(),
                        bottom: $(window).height() - $wnd.offset().top - $wnd.outerHeight()
                    }));

                    _mto = {left: 0, right: 0, top: 0, bottom: 0};

                    if ($('.lk-application--header').length !== 0) {
                        _mto.left = parseInt($('.lk-application--header').css('padding-left'));
                        _mto.right = parseInt($('.lk-application--header').css('padding-right'));
                        _mto.top = $('.lk-application--header').outerHeight();
                    }

                    if ($('.lk-application--footer').length !== 0) {
                        _mto.bottom = $('.lk-application--footer').outerHeight();
                    }

                    $('.lk-window--frame').animate(_mto, _WINDOW_ANIMATION * GLOBAL_ANIMATE, function () {
                        $wnd.css({width: '', height: ''}).css(_mto);
                        $('.lk-window--frame').remove();
                        _fixbody($wnd);
                    });

                    _options.windowState = 'wsMaximized';
                    $wnd.addClass('maximized');
                    $btn.find('.lk-icon').attr('class', 'lk-icon icon-maximized');
                    $btn.lkButton('enable');
                    _fixbody($wnd);
                }
            });
        },

        close: function () {
            return this.each(function () {
                var $this = $(this), _options = $this.data(options), $wnd = $this.closest('.lk-window');
                if ($wnd.length === 0)
                    return;
                $this.removeAttr('css style class').addClass('lk-dialog').appendTo($('body'));
                lk.delBkg(_options.id);
                $('#' + _options.taskbarid).find('*').off().removeData();
                $('#' + _options.taskbarid).removeData().remove();
                $wnd.find('*').off().removeData();
                $wnd.remove();
                $('.lk-window:last').addClass('lk-window-active');
                __setActiveWindow($('.lk-window-active .lk-window--body'));
            });
        },

        destroy: function () {
            return this.each(function () {
                var $this = $(this), _options = $this.data(options), $wnd = $this.closest('.lk-window');
                if ($wnd.length !== 0)
                    $this.lkWindow('close');
                $this.removeData();
            });
        }
    };

    $.fn.lkWindow = function (method) {
        if (methods[method])
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        else
        if (typeof method === 'object' || !method)
            return methods.create.apply(this, arguments);
        else
            $.error('lkWindow: method ' + method + ' not found');
    };

    function _fixbody(_wnd) {
        var $wnd = $(_wnd), $this = $wnd.find('.lk-window--body'), _options = $this.data(options),
                _header = $wnd.find('.lk-window--header'), _footer = $wnd.find('.lk-window--footer'), _method = _options.onResize;

        _options.left = parseInt($wnd.css('left'));
        _options.top = parseInt($wnd.css('top'));
        _options.width = parseInt($wnd.css('width'));
        _options.height = parseInt($wnd.css('height'));

        $wnd.find('.lk-window--body').css({left: 0, right: 0, top: 0, bottom: 0});

        if (_header.length > 0)
            $wnd.find('.lk-window--body').css({
                top: parseInt(_header.css('height'))
            });

        if (_footer.length > 0)
            $wnd.find('.lk-window--body').css({
                bottom: parseInt(_footer.css('height'))
            });
       
        if(_method){
            _method.call($wnd);
        }
    }

    function __window__mousedown(event) {

        event.preventDefault();

        if (event.button !== 0)
            return;
        var $wnd = $(event.target).closest('.lk-window'), $this = $wnd.find('.lk-window--body'), _options = $this.data(options);

        if (_options.windowState === 'wsMaximized')
            return;
        if (_options.windowState === 'wsMinimized')
            return;

        $wnd.addClass('update-action');
        $('.lk-window').off('mousedown focusin');

        _options._control = event.target;
        if ($(event.target).css('cursor') !== 'auto')
            $('body').addClass($(event.target).css('cursor'));
        __window__focusin(event);
        _options.o = {
            px: event.pageX,
            py: event.pageY,
            wx: parseInt($wnd.css('left')),
            wy: parseInt($wnd.css('top')),
            ww: parseInt($wnd.css('width')),
            wh: parseInt($wnd.css('height'))
        };
        $('.lk-window').addClass('unselectable');
        $(window).on({
            mousemove: __window__mousemove
        });
        $(document).on({
            'mouseup mouseleave': __window__mouseup
        });
    }

    function __window__mousemove(event) {
//        event.stopPropagation();

        var $wnd = $('.update-action'),
                $wnd_frame = $('.lk-window--frame');
        if ($wnd_frame.length === 0) {
            lk.setFormBack();
            $('body').append($('<div>').addClass('lk-window--frame').css({
                left: parseInt($wnd.css('left')),
                top: parseInt($wnd.css('top')),
                width: parseInt($wnd.css('width')),
                height: parseInt($wnd.css('height'))
            }));
        }

//        if ($wnd.length === 0) return;

        var $this = $wnd.find('.lk-window--body'), _options = $this.data(options),
                _control = _options._control,
                mtx = event.pageX,
                mty = event.pageY,
                _cw = parseInt($wnd_frame.css('width')),
                _ch = parseInt($wnd_frame.css('height')),
                _mw = parseInt($(window).width()),
                _mh = parseInt($(window).height());
                var _fixt = 0, _fixl = 0, _fixr = 0, _fixb = 0;
                if ($('.lk-application--header').length !==0) {
                    _fixt = $('.lk-application--header').outerHeight();
                    _fixt = (_fixt === undefined) ? 0 : _fixt;
                    _fixl = parseInt($('.lk-application--header').css('padding-left'));
                    _fixl = (_fixl === undefined) ? 0 : _fixl;
                    _fixr = parseInt($('.lk-application--header').css('padding-right'));
                    _fixr = (_fixr === undefined) ? 0 : _fixr;
                };
                if ($('.lk-application--footer').length !== 0 ) {
                    _fixb = $('.lk-application--footer').outerHeight();
                    _fixb = (_fixb === undefined) ? 0 : _fixb;
                };

        if ($(_control).hasClass('lk-window--header')) {
            $('body').addClass('all-scroll');

            if ((mtx - _options.o.px + _options.o.wx >= _fixl) && (mtx - _options.o.px + _options.o.wx + _cw <= _mw - _fixr))
                $wnd_frame.css({
                    left: mtx - _options.o.px + _options.o.wx
//                    top: mty - _options.o.py + _options.o.wy
                });

            if ((mty - _options.o.py + _options.o.wy >= _fixt) && (mty - _options.o.py + _options.o.wy + _ch <= _mh - _fixb))
                $wnd_frame.css({
//                    left: mtx - _options.o.px + _options.o.wx
                    top: mty - _options.o.py + _options.o.wy
                });
        }

        if ($(_control).hasClass('border-right'))
            if (mtx <= _mw - _fixr)
                $wnd_frame.css({
                    width: mtx - _options.o.px + _options.o.ww
                });

        if ($(_control).hasClass('border-left'))
            if (mtx >= _fixl)
                $wnd_frame.css({
                    width: _options.o.ww - (mtx - _options.o.px),
                    left: _options.o.wx + (mtx - _options.o.px)
                });

        if ($(_control).hasClass('border-bottom'))
            if (mty <= _mh - _fixb)
                $wnd_frame.css({
                    height: _options.o.wh + (mty - _options.o.py)
                });

        if ($(_control).hasClass('border-top'))
            if (mty >= _fixt)
                $wnd_frame.css({
                    height: _options.o.wh - (mty - _options.o.py),
                    top: _options.o.wy + (mty - _options.o.py)
                });
    }

    function __window__mouseup() {
        var $wnd = $('.update-action'),
                $wnd_frame = $('.lk-window--frame');
        if (($wnd.length !== 0) && ($wnd_frame.length !== 0)) {
            $wnd.css({
                left: $wnd_frame.css('left'),
                top: $wnd_frame.css('top'),
                width: $wnd_frame.css('width'),
                height: $wnd_frame.css('height')
            });
        }
        lk.delFormBack();
        $wnd_frame.remove();
        $('.update-action').removeClass('update-action');
        $('body').removeClass('all-scroll w-resize e-resize n-resize s-resize nw-resize ne-resize sw-resize se-resize');
        $('.lk-window').removeClass('unselectable');
        $(window).off('mousemove');
        $(document).off('mouseup').off('mouseleave');
        _fixbody($wnd);
        /*
         $('.lk-window').on({
         'mousedown focusin': __window__focusin
         });
         */
    }

    function __taskbar__click(event) {
        var $wnd = $('#' + $(event.target).closest('.item').data(options).formid), $this = $wnd.find('.lk-window--body'), _options = $this.data(options);
        $('.lk-taskbar .item').removeClass('active');
        $(event.target).closest('.item').addClass('active');

        if ((_options.id === $('.lk-window-active').attr('id')) || (_options.windowState === 'wsMinimized'))
            methods['minimize'].apply($this);
        __setActiveWindow($this);
    }

    function __setActiveWindow(_this) {
        var $this = $(_this), _options = $this.data(options), $wnd = $(_this).closest('.lk-window'), $taskBar_item; // = $('#' + _options.taskbarid);

        if ($this.closest('.lk-window').length === 0)
            return;
        if ((_options.modal === true) || (_options.modal === 'true'))
            return;
        $('.lk-window:not(#' + $wnd.attr('id') + ')').insertBefore($wnd).removeClass('lk-window-active');

        if ($wnd.length !== 0) {
            $taskBar_item = $('#' + _options.taskbarid);
            if ($taskBar_item.length !== 0) {
                $('.lk-taskbar .item').removeClass('active');
                $taskBar_item.addClass('active');
            }
        }
        $wnd.addClass('lk-window-active');
    }

    function __window__focusin(event) {
        __setActiveWindow($(event.target).closest('.lk-window').find('.lk-window--body'));
    }

})(jQuery);