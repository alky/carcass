;
(function ($) {
    var options = 'lkButtonSet';
    var BUTTONSET_ANIMATE = 400;
    var methods = {
        create: function (options_set) {
            return this.each(function () {
                var $this = $(this), _options;
                if (!($this.data(options))) {
                    $this.data(options, $.extend({timeout: 5000, closeOnClick: true}, options_set));
                    _options = $this.data(options);
                    $this.addClass('lk-buttonset');
                    if (_options.buttons) {
                        for (var i in _options.buttons) {
                            $this.append($('<div>').addClass('lk-buttonset-btn').css({position: 'absolute', left: _options.buttons[i].x, top: _options.buttons[i].y, display: 'none'})
                                    .append($('<button>').attr('btn-no', i).attr('title', _options.buttons[i].hint).lkButton({
                                        type: 'fab-mini',
                                        icon: _options.buttons[i].icon,
                                        backgroundColor: _options.buttons[i].backgroundColor,
                                        rippleColor: _options.buttons[i].rippleColor,
                                        onClick: __click
                                    })).fadeIn(BUTTONSET_ANIMATE * GLOBAL_ANIMATE));
                        }
                    }
                    if (_options.timeout !== 0) {
                        _options.timer = setTimeout(function () {
                            methods['close'].apply($('.lk-buttonset'));
                        }, _options.timeout);
                    }
                }
            });
        },

        close: function () {
            return this.each(function () {
                var $this = $(this), _options = $this.data(options);
                clearTimeout(_options.timer);
                $this.find('div').fadeOut(BUTTONSET_ANIMATE * GLOBAL_ANIMATE, function () {
                    $($this.find('*')).off('*').removeData().remove();
                    $this.off('*').removeData().remove();
                });
            });
        }
    };

    $.fn.lkButtonSet = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else {
            if (typeof method === 'object' || !method) {
                return methods.create.apply(this, arguments);
            } else {
                $.error('lkButtonSet: method ' + method + ' not found');
            }
        }
    };

    function __click(event) {
        event.stopPropagation();
        var $btn = $(event.target).closest('button'),
                $this = $(event.target).closest('.lk-buttonset'),
                _options = $this.data(options),
                idx = $btn.attr('btn-no'),
                _m = _options.buttons[idx].onClick;
        if (_m) {
            _m.call($btn, event);
        }
        if (_options.closeOnClick === true) {
            methods['close'].apply($this);
        }
    }

})(jQuery);