<?php

namespace Controller\Yamaps;
//use InfLK\mysqli;

/**
 * Description of GuestPage
 *
 * @author alk
 */
//echo (__DIR__);


class GuestPage extends \System\Controller 
{
    
//    private $connection;
//    
//    private $config;
    
    private $lifetime = '180'; // Keep guest pages for 180 days 
    
    
    function index() 
    {
  
        $this->removeOldPages();
        
        if (isset($this->request->post['f'])) {
        
            return $this->save($_POST['f']);
        }
        
        if (isset($this->request->get['pid']) && isset($this->request->get['num'])) {
        
            return $this->getSaved($this->request->get['pid'], $this->request->get['num']);
        }
        
        
    }
    
    
    private function removeOldPages() 
    {
         date_default_timezone_set('Europe/Moscow');
         
         $dn = date("Y-m-d");
         
         $lifetime = $this->lifetime;
         
         $limit = "-$lifetime days";
         
         $days_ago = date('Y-m-d', strtotime($limit, strtotime($dn)));
         
         $sql = "DELETE FROM guestSavedPages WHERE date_added < '$days_ago'";
         
         $this->db->query($sql);
         
    }
    
    function save($data) 
    {
        $html = $this->db->escape(serialize($data));
//        $html = serialize($data);
//$this->log->write($html); 
        $sql = "INSERT INTO guestSavedPages SET html = '$html', date_added = NOW()";
        
        $this->db->query($sql);
        
        $id = $this->db->getLastId();
        
        $hash = crypt('pageid'.$id, 'dghfFdstg');
        
        $update = "UPDATE guestSavedPages SET hash = '$hash' WHERE id = '$id'";
        
        $this->db->query($update);
        
        $urn = 'pid='.$id.'&num='.$hash;
        return $urn;
    }
    
    function getSaved($id, $hash)
    {
        $sql = "SELECT html FROM guestSavedPages WHERE id = '$id' AND hash = '$hash'";
        
        $result = $this->db->query($sql);
        
        return $result->row;
    }
    
    
    
    
}

