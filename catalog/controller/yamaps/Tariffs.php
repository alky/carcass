<?php

namespace Controller\Yamaps;

class Tariffs extends \System\Controller
{
private $tarif_id_name = array(
    '1975746' => 'PRICESTANDART',//
    '1975748' => 'PRICEPERSONAL',//
    '1975747' => 'PRICESPECIAL',//
    '1975749' => 'PRICEASMAP',
    '2816654' => 'PRICEPREPAY',//
    '15846216' => 'PRICERUB',//
    '69091507' => 'PRICEOSOB',//
    '78983640' => 'PRICEEXCL', 
    '77609457' => 'PREPAY_LE5000',//
    '77609475' => 'PREPAY_LE10000',//
    '77609491' => 'PREPAY_LE20000',//
    '77609535' => 'PREPAY_G20000',//
    '77609702' => 'CRED14LIMIT_LE5000',//
    '77609742' => 'CRED14LIMIT_LE10000',//
    '77609761' => 'CRED14LIMIT_LE20000',//
    '77609777' => 'CRED14LIMIT_G20000',//
    '77609890' => 'CRED20PLUS_LE5000',//
    '77609900' => 'CRED20PLUS_LE10000',//
    '77609905' => 'CRED20PLUS_LE20000',//
    '77609916' => 'CRED20PLUS_G20000',//
);

private $tarif_groups = array(
    'Особая предоплата' => array(77609457, 77609475, 77609491,77609535),
    'Особая кредит 12,14' => array(77609702,77609742,77609761,77609777),
    'Особая кредит 20+' => array(77609890,77609900,77609905,77609916),
    'Прочие' => array(1975746,1975748,1975747,2816654,15846216,69091507)
    
);

    public function index() {
        
        $cache_file = 'tariffs';
             
        $tariffs = $this->cache->get($cache_file);        
        
        if (!$tariffs) {
        
            $tariffs = $this->getHtmlSelectOptions();

            $this->cache->set($cache_file, $tariffs);
        
        }
        
        return $tariffs;
        
    }

    private function getHtmlSelectOptions() 
    {
        $current_tarif_id = '78983640';
        $tarif_name = $this->tarif_id_name;
            
            
        if (isset($tarif_name[$current_tarif_id])) {
                 $selected_name = $tarif_name[$current_tarif_id];
        } else {
            $selected_name = 'PRICEEXCL';
        }        

        $tariffs = array();
        $options = $this->_getTariffs();
        $caption = array();
        $groups = $this->tarif_groups;
        
        foreach ($options as $option) {
            
            $ingroup = false;
            
            foreach ($groups as $gname=>$group) {
                
//                $v = array_search($option['id'], $group);
//                if ($v !== false) {
                  if (in_array($option['id'], $group)) {  
                    $caption[$gname]['items'][] = array(
                        'caption' => $option['name'],
                        'mid' => $option['id'],
                        'name' => $option['view_clmn']
                    );
                    
                    $ingroup = true;
                    break;
                }
            }    
            
            if (!$ingroup) {
                $tariffs['items'][] = array(
                    'caption' => $option['name'],
                    'mid' => $option['id'],
                    'name' => $option['view_clmn'],
                    );
            }
        }
        
        foreach ($caption as $i=>$d) {
            array_unshift($tariffs['items'], array('caption' => $i,'items' => $d['items']));
        }
        
        $data['tariffs'] = $tariffs;

        $ftypes = array();
        foreach ($this->_getFuelTypes() as $type) {
            $ftypes['items'][] = array(
                  'caption' => $type['name'],
                  'mid' => $type['id'],
            );            
        }        
        
        
        $data['ftypes'] = $ftypes;
        
        return $data;
    }    
    
    private function _getTariffs()
    {
        $sql = 'SELECT ID, CODE, NAME FROM ZDB.LIPRICE#V ORDER BY ID';

        $result = $this->oracle->query($sql);
        
        $known_tarifs = $this->tarif_id_name;

        $rowset = array();
        
        foreach ($result->rows as $tarif) {
            
            if (isset($known_tarifs[$tarif['ID']])) {
                
                $rowset[] = array(
                  'id' => $tarif['ID'],
                  'name' => $tarif['NAME'],
                  'view_clmn' => $known_tarifs[$tarif['ID']]
                );
            }
            
        }
          
        return $rowset;
    }

    private function _getFuelTypes()
    {
        $sql = 'SELECT PARENTID, PARENTID_FULLNAME FROM ZDB.LIBPRODUCTGROUP#V GROUP BY PARENTID, PARENTID_FULLNAME';

        $result = $this->oracle->query($sql);
        
        $rowset = array();
        
        foreach ($result->rows as $type) {
            
            if ($type['PARENTID_FULLNAME'] != 'доп. услуги') {
                
                $rowset[] = array(
                  'id' => $type['PARENTID'],
                  'name' => $type['PARENTID_FULLNAME'],
                );
            }
            
        }
          
        return $rowset;
    }
    
}

