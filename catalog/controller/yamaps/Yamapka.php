<?php

namespace Controller\Yamaps;

/**
 * Description of Yamapka
 *
 * @author alk
 */
class Yamapka extends \System\Controller
{
    private $tarif_id_name = array(
                '1975746' => 'PRICESTANDART',
                '1975748' => 'PRICEPERSONAL',
                '1975747' => 'PRICESPECIAL',
                '1975749' => 'PRICEASMAP',
                '2816654' => 'PRICEPREPAY',
                '15846216' => 'PRICERUB',
                '69091507' => 'PRICEOSOB',
                '78983640' => 'PRICEEXCL', 
                '77609457' => 'PREPAY_LE5000',
                '77609475' => 'PREPAY_LE10000',
                '77609491' => 'PREPAY_LE20000',
                '77609535' => 'PREPAY_G20000',
                '77609702' => 'CRED14LIMIT_LE5000',
                '77609742' => 'CRED14LIMIT_LE10000',
                '77609761' => 'CRED14LIMIT_LE20000',
                '77609777' => 'CRED14LIMIT_G20000',
                '77609890' => 'CRED20PLUS_LE5000',
                '77609900' => 'CRED20PLUS_LE10000',
                '77609905' => 'CRED20PLUS_LE20000',
                '77609916' => 'CRED20PLUS_G20000',
            );
    private $tarif_percent = array(
        '78983640',
    );
    
    private $ftype_category = array(124320, 124322, 259093, 401207, 'addblue');
        
    private $icon_presets = array (
            0 => 'islands#grayIcon',
            1 => 'islands#lightBlueIcon',
            2 => 'islands#oliveIcon',
            3 => 'islands#yellowIcon',
            4 => 'islands#pinkIcon',
            5 => 'islands#blueIcon',
            6 => 'islands#greenIcon',
            7 => 'islands#darkGreenIcon',
            8 => 'islands#violetIcon',
            9 => 'islands#orangeIcon',
            10 => 'islands#darkOrangeIcon',
            11 => 'islands#redIcon',
            12 => 'islands#brownIcon',
            13 => 'islands#darkBlueIcon',
            14 => 'islands#nightIcon',
            'extra' => 'islands#blackIcon'
        );


    private $current_tariff = '';
    
    private $addblue = false;
    
    function index()
    {
        $param = $this->request->get;

        if ($param['t'] == 'p') {
          
                $tmp = explode(',', $param['bbox']);

                if (isset($param['maxprice'])) {
                        $maxprice = (float)$param['maxprice'];
                } else {
                    $maxprice = null;
                }

                $data = array(
                    'gpslat-f' => $tmp[0],
                    'gpslat-t' => $tmp[2],
                    'gpslng-f' => $tmp[1],
                    'gpslng-t' => $tmp[3],
                    'tariff' => isset($param['tariff']) ? $param['tariff'] : null,
                    'ftype' => ( isset($param['ftype']) && in_array($param['ftype'], $this->ftype_category) ) ? $param['ftype'] : null,
                    'maxprice' => $maxprice
                );

                /*Если указываем свой колбэк filterscb*/
                if (isset($param['filterscb']) && $param['filterscb'] != '') {
                    $callback = $param['filterscb'];
                } else {
                    $callback = $param['callback'];
                }

                return array(
                    'callback' => $callback,
                    'data' => $this->_get($data)
                );
            

        } elseif ($param['t'] == 'i') {
                
            $data = array(
                    'id' => $param['id'],
                    'tariff' => ($param['tariff'] && $param['tariff'] != 'all') ? $param['tariff'] : null,
                    'ftype' => ( isset($param['ftype']) && in_array($param['ftype'], $this->ftype_category) ) ? $param['ftype'] : null,
                    );
            
            return $this->getStation($data);
                
        } 
        
    }    
    
    function _get($data)
    {
            
            $this->load->model('yamaps/Yamapka');
        
            $tarif = '';
            
            $this->current_tariff = null;
            
            if ($data['tariff']) {
                
                $current_tarif_id = $data['tariff'];
                $tarif_name = $this->tarif_id_name;

                 if (isset($tarif_name[$current_tarif_id])) {
                     
                     $this->current_tariff = $current_tarif_id;
                     
                     $tarif = $tarif_name[$current_tarif_id];
                     
                 }               
            }
            
            if ($data['ftype']) {
                $ftype = $data['ftype'];
            } else {
                $ftype = ''; 
            }

            if ($data['maxprice']) {
                $maxprice = $data['maxprice'];
            } else {
                $maxprice = ''; 
            }            
            
             $pattern = '/[.,]/';
             $cache_file = 'stations_' . $tarif . '_' . $ftype . '_' . $maxprice . '_' . preg_replace($pattern, '', $data['gpslat-f']) . '_'. preg_replace($pattern, '', $data['gpslat-t']); 
             $cache_file .= '_'. preg_replace($pattern, '', $data['gpslng-f']) . '_'. preg_replace($pattern, '', $data['gpslng-t']);
             
             $collection = $this->cache->get($cache_file);
             
             if (!$collection) {
                 
                 if ($ftype == 'addblue') {

                     $this->addblue = true;

                     $result = $this->model_yamaps_Yamapka->getAllAddBlue($data);
                     
                 } elseif ($tarif) {
                     $data['tariff_name'] = $tarif;
                     
                     $result = $this->model_yamaps_Yamapka->getFilteredStations($data);
                     
                 } else {
                     $result = $this->model_yamaps_Yamapka->getAllStations($data);
                 }
                 
                 if (!empty($result->rows)) {

                    $collection = $this->createCollection($result); 

                    $this->cache->set($cache_file, $collection);

                 }
             }
             
          return $collection;
    }
    
    private function createCollection($data)
    {
        $collection = array('type' => 'FeatureCollection');
        
            foreach ($data->rows as $row) {
              
              $this->fixTitle($row['FULLNAME']);

              $coords = array(floatval(str_replace(',', '.', $row['GPSLAT'])), floatval(str_replace(',', '.', $row['GPSLNG'])));


              $geometry = array(
                  'type' => 'Point',
                  'coordinates' => $coords
                  );
              
              if ($this->current_tariff) {
                $discount_data = $this->getDiscount($row);
                $preset = $this->iconPreset($row);
              } else {
                 $discount_data = array();
                 $preset = null;
              }
              
              if ($this->addblue) {
                $presets = $this->icon_presets;
                  $preset = $presets['extra'];
              }
              
              $props = array(
//                'balloonContent'    => "Содержимое балуна",
//                'clusterCaption'    => isset($row['WEBCOMMENT']) ? $row['WEBCOMMENT'] : '',
                'hintContent'       => array(
                                        'i_name'=> $row['NAME'],
                                        'i_discount' => isset($discount_data['value']) ? $discount_data['value'] : '',
                                        'i_type' => isset($discount_data['type']) ? $discount_data['type'] : '',
                                        ),
                'discount'          =>  isset($discount_data['value']) ? $discount_data['value'] : '',

              );

              if ($preset) {
                  $opts = array('preset' => $preset,'hintContentLayout' => 'fshint');
              } else {
                  $opts = array('hintContentLayout' => 'fshint');
              }

              $collection['features'][] = array(
                     'type' => 'Feature',
                     'id' => (int)$row['ID'],
                     'geometry' => $geometry,
                     'properties' => $props,
                     'options' => $opts
              );



            }
            
        return $collection;    
    }
    
    

    function iconPreset($data)
    {

        $presets = $this->icon_presets;
        $discount_data = $this->getDiscount($data);
        
       if (empty($discount_data) || $discount_data['value'] <= 0 ) {
           return $presets[0];
       } else {

           if ($discount_data['type'] == '%') {
               if ($discount_data['value'] > 14 ) {
               $preset = $presets['extra'];
               } else {
               $preset = $presets[round($discount_data['value'], 0, PHP_ROUND_HALF_UP)];    
               }
           } else {
               $preset = $presets[6];
           }
           
            return $preset;
       }
        
       
    }
    
    /*
     * Returns array (
     * discount type
     * duscount value )
     */
    
    private function getDiscount($data)
    {
    
        $discount_data = array();
        
        $current_tarif_id = $this->current_tariff;

        $tarif_name = $this->tarif_id_name;

         if (isset($tarif_name[$current_tarif_id])) {
             $column_TarifName = $tarif_name[$current_tarif_id];
             
             if (isset($data['PRICENAL']) && $data['PRICENAL'] != 0){
                 $base_value = floatval(str_replace(',','.',$data['PRICENAL']));
                 $discounted_value = floatval(str_replace(',','.',$data[$column_TarifName]));

                 if (in_array($current_tarif_id, $this->tarif_percent)) {
                     $discount = (1 - $discounted_value/$base_value)*100;
                     $discount_data = array(
                         'type' => '%',
                         'value' => round($discount, 2, PHP_ROUND_HALF_UP)
                     );
                 } else {
                     $discount = $base_value - $discounted_value;
                     $discount_data = array(
                         'type' => 'Rub',
                         'value' => round($discount, 2, PHP_ROUND_HALF_UP)
                     );
               }
            }
         }
        return $discount_data;
        
    }
    
    
    private function getStation($data) 
    {
        $current_tarif_id = $data['tariff'];
        $tarif_name = $this->tarif_id_name;
        $this->load->model('yamaps/Yamapka');

        if ($current_tarif_id && isset($tarif_name[$current_tarif_id])) {
            
            $this->current_tariff = $current_tarif_id;
            
            $data['tarif_name'] = $tarif_name[$current_tarif_id];
            
            $data['tarif_percent'] = in_array($current_tarif_id, $this->tarif_percent);
            
        }
        
        $result = $this->model_yamaps_Yamapka->getStationData($data);
        
        return $result;
        
    }
    
    

    
    private function _floor($input)
    {
        $intValue = intval($input);
        $frac = $input - $intValue;
        if ($frac < 0.5) return $intValue;
        return $intValue + 0.5;
    }
    
    private function fixTitle(&$c) {
        $c = preg_replace('/^.*[[:space:]]/uU', '', trim($c));
    }    
    
    
}
