<?php

namespace Controller\Common;

//class ControllerCommonHeader extends Controller {
class Header extends \System\Controller {    
	public function index() {

                
		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
//			$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
		}
                
                $data = array();
                
		$data['title'] = $this->document->getTitle();
		$data['base'] = $server;
		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts();

		$data['name'] = 'Карта АЗС';
                $data['direction'] = 'ltr';
                $data['lang'] = 'ru';

//		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
//			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
//		} else {
//			$data['logo'] = '';
//		}
                
		$data['og_url'] = (isset($this->request->server['HTTPS']) ? HTTPS_SERVER : HTTP_SERVER) . substr($this->request->server['REQUEST_URI'], 1, (strlen($this->request->server['REQUEST_URI'])-1));
		$data['og_image'] = $this->document->getOgImage();


		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/header.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/header.tpl', $data);
		} else {
			return $this->load->view('default/template/common/header.tpl', $data);
		}
	}
}
