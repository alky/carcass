<?php

namespace Controller\Common;

//class ControllerCommonHome extends Controller {
class Home extends \System\Controller {    
	public function index() {
		$this->document->setTitle('Карта АЗС Инфорком');
		$this->document->setDescription('Карта АЗС Инфорком');
		$this->document->setKeywords('Карта АЗС Инфорком');
                
                
                $this->document->addScript('catalog/view/javascript/jquery/jquery-3.2.1.min.js');
                $this->document->addScript('https://api-maps.yandex.ru/2.1.56/?lang=ru_RU');
                $this->document->addScript('catalog/view/javascript/lk/moment.min.js');
                $this->document->addScript('catalog/view/javascript/lk/moment-locales.min.js');
                $this->document->addScript('catalog/view/javascript/lk/numeral.min.js');
                $this->document->addScript('catalog/view/javascript/lk/numeral-locales.min.js');                
                $this->document->addScript('catalog/view/javascript/lk/lkButton.js');
                $this->document->addScript('catalog/view/javascript/lk/lkButtonSet.js');
                $this->document->addScript('catalog/view/javascript/lk/lkPopup.js');
                $this->document->addScript('catalog/view/javascript/lk/lkEditor.js');
                $this->document->addScript('catalog/view/javascript/lk/lkWindow.js');
                $this->document->addScript('catalog/view/javascript/lk/lkUtil.min.js');
                $this->document->addScript('catalog/view/javascript/pie-chart-clusterer.min.js');
                $this->document->addScript('catalog/view/javascript/map.js');
                $this->document->addScript('catalog/view/javascript/excel/export.js');
                
                
                $this->document->addStyle('catalog/view/theme/default/stylesheet/application-normal.css');
                $this->document->addStyle('catalog/view/theme/default/stylesheet/application-extra.css');
                
		if (isset($this->request->get['route'])) {
			$this->document->addLink(HTTP_SERVER, 'canonical');
		}
                $data = array();
                
		$data['footer'] = $this->load->controller('common/footer');
                $data['header'] = $this->load->controller('common/header');
                

                
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/home.tpl')) {
                        $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/home.tpl', $data));
                } else {
                        $this->response->setOutput($this->load->view('default/template/common/home.tpl', $data));
                }
	}
        
        /* Аякс запрос фильтров
         * 
         */

         
        public function getTariff(){
            
            $response = $this->load->controller('yamaps/Tariffs');

            $this->response->addHeader('Content-Type: application/json');

            $this->response->setOutput(json_encode($response));            
            
        }
        
        /* Обработчик Яндекс LoadingManagerObject
        * 
        */        
        
       public function lom(){
           
            $response = $this->load->controller('yamaps/Yamapka');
            
            /*Основной запрос Object manager возвращаем обернутый в функицю (JSONP)*/
            if (isset($this->request->get['t']) && $this->request->get['t'] == 'p') {
            
                $this->response->addHeader('Content-Type: application/json');
                
                $this->response->setOutput($response['callback'].'('.json_encode($response['data']).');');
                
            }
            
            /*Все остальные - простой JSON*/
            if (isset($this->request->get['t']) && $this->request->get['t'] == 'i') {
            
                $this->response->addHeader('Content-Type: application/json');
                
                $this->response->setOutput(json_encode($response));
                
            }            
            
        }
        
        public function guestmap() {
            
            $response = $this->load->controller('yamaps/GuestPage');
            
            if (isset($this->request->post['f'])) {
            $this->response->addHeader('Content-Type: application/json');

            $this->response->setOutput(json_encode($response));            
            }
            
            if (isset($this->request->get['pid']) && isset($this->request->get['num'])) {

                $this->document->addScript('catalog/view/javascript/jquery/jquery-3.2.1.min.js');
                $this->document->addScript('https://api-maps.yandex.ru/2.1.56/?lang=ru_RU');
                $this->document->addScript('catalog/view/javascript/lk/moment.min.js');
                $this->document->addScript('catalog/view/javascript/lk/moment-locales.min.js');
                $this->document->addScript('catalog/view/javascript/lk/numeral.min.js');
                $this->document->addScript('catalog/view/javascript/lk/numeral-locales.min.js');                
                $this->document->addScript('catalog/view/javascript/lk/lkButton.js');
                $this->document->addScript('catalog/view/javascript/lk/lkButtonSet.js');
                $this->document->addScript('catalog/view/javascript/lk/lkPopup.js');
                $this->document->addScript('catalog/view/javascript/lk/lkEditor.js');
                $this->document->addScript('catalog/view/javascript/lk/lkWindow.js');
                $this->document->addScript('catalog/view/javascript/lk/lkUtil.min.js');
                $this->document->addScript('catalog/view/javascript/pie-chart-clusterer.min.js');
                
                
                $this->document->addStyle('catalog/view/theme/default/stylesheet/application-normal.css');
                $this->document->addStyle('catalog/view/theme/default/stylesheet/application-extra.css');

                
                $data['footer'] = $this->load->controller('common/footer');
                $data['header'] = $this->load->controller('common/header');
                
                $data['json'] = unserialize($response['html']);
                
                $this->response->setOutput($this->load->view('default/template/common/guest_page.tpl', $data));
                
            }
        }
        
}