<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DbOracle
 *
 * @author alk
 */
class DbOracle {

    /** @var \db $db */
    private static $_oracle;

    private static $_config;

    static function config($id)
    {
        if (!isset(self::$_config)) {
            self::$_config = include(__DIR__ . '/../config_oracle.php');
        }

        return self::$_config[$id];
    }

    static function oracle() {

        if (!self::$_oracle) {
            $cfg = self::config('oracle');
            $dsn = $cfg['db_host'] . ':' . $cfg['db_port'] . '/' . $cfg['db_name'];
            self::$_oracle = oci_connect($cfg['db_user'], $cfg['db_pass'], $dsn, 'AL32UTF8'); //, 'AL32UTF8'
            // Russian.AL32UTF8 | CL8MSWIN1251
            // self::$_oracle = new \PDO('oci:dbname='.$dsn /*.';charset=CL8MSWIN1251'*/, $cfg['db_user'], $cfg['db_pass']);
        }

        return self::$_oracle;
        
    }
}
