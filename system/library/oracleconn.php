<?php

/**
 * Description of oracle_conn
 *
 * @author alk
 */
class OracleConn {

    function connection() {
        
        return DbOracle::oracle();
        
    }

    function query($sql, $bindname = array()) {

        $link = $this->connection();


        $query = oci_parse($link, $sql);


        oci_execute($query);

        $data = array();

        while ($row = oci_fetch_array($query, OCI_ASSOC)) {
            $data[] = $row;
        }

        $result = new \stdClass();
        $result->row = isset($data[0]) ? $data[0] : array();
        $result->rows = $data;

        oci_free_statement($query);
        oci_close($link);
        return $result;
    }

    function procedure($name, $param) {

        $link = $this->connection();

        $query = oci_parse($link, $name);

        foreach ($param as $key => $val) {

            oci_bind_by_name($query, $key, $param[$key], 255);
        }

        if (oci_execute($query) == true) {
            $data = $param;
        } else {
            $data = oci_error();
        }

        oci_free_statement($query);
        oci_close($link);
        return $data;
    }

}
