<?php
class Config {
	private $data = array(
            'config_url' => HTTP_SERVER,
            'config_ssl' => HTTPS_SERVER,
            'config_secure' => false,
            'config_error_filename' => 'erorrs.txt',
            'config_error_display' => false,
            'config_error_log' => true,
            'config_compression' => 0,
            'config_encryption' => 'kshdgfkjsdhfllkasdzcxfds'
        );
        
	public function get($key) {
		return (isset($this->data[$key]) ? $this->data[$key] : null);
	}

	public function set($key, $value) {
		$this->data[$key] = $value;
	}

	public function has($key) {
		return isset($this->data[$key]);
	}

	public function load($filename) {
		$file = DIR_CONFIG . $filename . '.php';

		if (file_exists($file)) {
			$_ = array();

			require($file);

			$this->data = array_merge($this->data, $_);
		} else {
			trigger_error('Error: Could not load config ' . $filename . '!');
			exit();
		}
	}
}